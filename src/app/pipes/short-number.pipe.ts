import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Pipe({
  name: "shortNumber"
})
export class ShortNumberPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
    // Do nothing.
  }

  transform(value: number) {
    if (value >= 1e6) {
      return (value / 1e6).toFixed(1) + "m";
    }

    if (value >= 1e3) {
      return (value / 1e3).toFixed(1) + "k";
    }

    return value.toString();
  }

}
