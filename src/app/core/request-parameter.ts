export class RequestParameter {

  public static readonly HASHTAG_ID = "hashtagId";
  public static readonly INPUT = "input";
  public static readonly NOTES = "notes";
  public static readonly POST_TYPE = "postType";
  public static readonly PROMPT_ID = "promptId";
  public static readonly PUBLISH_STATUS = "publishStatus";
  public static readonly SEGMENT = "segment";
  public static readonly TOPIC_ID = "topicId";
  public static readonly USER_ID = "userId";

}
