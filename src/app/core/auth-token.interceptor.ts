import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {

  constructor(private firebaseClient: FirebaseClient) {
    // Do nothing.
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.firebaseClient.getAuthState().pipe(switchMap((firebaseUser) => {
      if (firebaseUser) {
        const authToken = firebaseUser.uid;  // TODO: Replace this with actual firebase ID token.

        request = request.clone({
          setHeaders: {
            Authorization: "Bearer " + authToken
          }
        });
      }

      return next.handle(request);
    }));
  }
}
