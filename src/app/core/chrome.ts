import { Injectable } from "@angular/core";
import { EventBus } from "@app/core/event-bus";
import { Navigation } from "@app/core/navigation";
import { Routing } from "@app/core/routing";
import { Seo } from "@app/core/seo";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { LoadingController, ToastController } from "@ionic/angular";
import { NGXLogger } from "ngx-logger";
import { from } from "rxjs";
import { flatMap } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class Chrome {

  constructor(public eventBus: EventBus,
              public logger: NGXLogger,
              public navigation: Navigation,
              public routing: Routing,
              public seo: Seo,
              public session: Session,
              public tracking: Tracking,
              private loadingController: LoadingController,
              private toastController: ToastController) {
    // Do nothing.
  }

  async presentLoading(message?: string, duration: number = 10000): Promise<void> {
    const loading = await this.loadingController.create({
      spinner: "dots",
      message: message,
      duration: duration,
      showBackdrop: false
    });

    return loading.present();
  }

  async dismissLoading(): Promise<boolean> {
    return this.loadingController.dismiss();
  }

  presentToast(message: string, position: "top" | "bottom" = "bottom", color = "dark", delay = 1200) {
    const toast = this.toastController.create({
      "message": message,
      "position": position,
      "duration": 1200,
      "color": color
    });

    setTimeout(() => {
      from(toast).pipe(flatMap((overlay) => overlay.present())).subscribe();
    }, delay);
  }

}
