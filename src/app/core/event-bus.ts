import { Injectable } from "@angular/core";
import { Event } from "@app/core/event";
import { Subject, Subscription } from "rxjs";
import { filter, map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class EventBus {

  private subject: Subject<any> = new Subject();

  on(event: Event, callback: any): Subscription {
    return this.subject.pipe(
      filter((message: any) => message.event === event),
      map((message: any) => message.data)
    ).subscribe(callback);
  }

  emit(event: Event, data?: any) {
    const message = {
      "event": event,
      "data": data
    };

    this.subject.next(message);
  }

}
