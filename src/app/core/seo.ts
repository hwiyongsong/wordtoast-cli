import { Injectable } from "@angular/core";
import { Meta, Title } from "@angular/platform-browser";

@Injectable({
  providedIn: "root"
})
export class Seo {

  public static readonly META_TITLE = "title";
  public static readonly META_DESCRIPTION = "description";
  public static readonly META_ROBOTS = "robots";
  public static readonly META_OG_TYPE = "og:type";
  public static readonly META_OG_TITLE = "og:title";
  public static readonly META_OG_DESCRIPTION = "og:description";
  public static readonly META_OG_IMAGE = "og:image";
  public static readonly META_OG_URL = "og:url";
  public static readonly META_OG_SITE_NAME = "og:site_name";
  public static readonly META_TWITTER_CARD = "twitter:card";
  public static readonly META_TWITTER_TITLE = "twitter:title";
  public static readonly META_TWITTER_DESCRIPTION = "twitter:description";
  public static readonly META_TWITTER_IMAGE = "twitter:image";
  public static readonly META_FB_APP_ID = "fb:app_id";

  constructor(private title: Title,
              private meta: Meta) {
  }

  setDefault(): Seo {
    this.clearMetaTags([
      Seo.META_DESCRIPTION,
      Seo.META_OG_TYPE,
      Seo.META_OG_DESCRIPTION,
      Seo.META_OG_IMAGE,
      Seo.META_OG_URL,
      Seo.META_TWITTER_DESCRIPTION,
      Seo.META_TWITTER_IMAGE
    ]);

    return this.setWebsite()
               .setTitle("WordToast")
               .setUrl("https://www.wordtoast.com")
               .setRobotFollowIndex()
               .setMetaTag(Seo.META_OG_SITE_NAME, "WordToast")
               .setMetaTag(Seo.META_TWITTER_CARD, "summary")
               .setMetaTag(Seo.META_FB_APP_ID, "2481694862087200");
  }

  setWebsite(): Seo {
    return this.setMetaTag(Seo.META_OG_TYPE, "website");
  }

  setArticle(): Seo {
    return this.setMetaTag(Seo.META_OG_TYPE, "article");
  }

  setTitle(title: string): Seo {
    if (title.indexOf("WordToast") < 0) {
      title += " - WordToast";
    }

    this.title.setTitle(title);
    return this.setMetaTag(Seo.META_TITLE, title)
               .setMetaTag(Seo.META_OG_TITLE, title)
               .setMetaTag(Seo.META_TWITTER_TITLE, title);
  }

  setDescription(description: string): Seo {
    return this.setMetaTag(Seo.META_DESCRIPTION, description)
               .setMetaTag(Seo.META_OG_DESCRIPTION, description)
               .setMetaTag(Seo.META_TWITTER_DESCRIPTION, description);
  }

  setHeroImageUrl(heroImageUrl: string): Seo {
    return this.setMetaTag(Seo.META_OG_IMAGE, heroImageUrl)
               .setMetaTag(Seo.META_TWITTER_CARD, "summary_large_image")
               .setMetaTag(Seo.META_TWITTER_IMAGE, heroImageUrl);
  }

  setUrl(url: string): Seo {
    return this.setMetaTag(Seo.META_OG_URL, url);
  }

  setRobotFollowIndex(): Seo {
    return this.setMetaTag(Seo.META_ROBOTS, "index,follow");
  }

  setRobotNoFollowNoIndex(): Seo {
    return this.setMetaTag(Seo.META_ROBOTS, "noindex,nofollow");
  }

  setMetaTag(name: string, value: string): Seo {
    this.meta.updateTag({
      name: name,
      content: value
    });

    return this;
  }

  clearMetaTags(names: string[]): Seo {
    for (const name of names) {
      this.meta.removeTag("name='" + name + "'");
    }

    return this;
  }

}
