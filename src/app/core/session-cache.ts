import { Injectable } from "@angular/core";
import { Topic } from "@app/entities/topic";
import { TopicService } from "@app/services/topic.service";
import { NGXLogger } from "ngx-logger";
import { forkJoin, Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class SessionCache {

  public topics: Array<Topic> = [];

  constructor(private logger: NGXLogger,
              private topicService: TopicService) {
    // Do nothing.
  }

  refreshAll(): Observable<any> {
    const sequence = forkJoin({
      topics: this.refreshTopics()
    });

    sequence.subscribe(() => {
      this.logger.debug("** Session cache refreshed");
    });

    return sequence;
  }

  refreshTopics(): Observable<Array<Topic>> {
    const sequence = this.topicService.queryTopics().pipe(map((topicsFeed) => topicsFeed.entities));

    sequence.subscribe((topics) => {
      this.topics = topics;
    });

    return sequence;
  }

}
