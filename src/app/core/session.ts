import { Injectable } from "@angular/core";
import { FirebaseClient } from "@app/clients/firebase.client";
import { SessionCache } from "@app/core/session-cache";
import { Tracking } from "@app/core/tracking";
import { User } from "@app/entities/user";
import { MyService } from "@app/services/my.service";
import { UserService } from "@app/services/user.service";
import { NGXLogger } from "ngx-logger";
import { forkJoin, Observable, of } from "rxjs";
import { flatMap, map, share } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class Session {

  public user: User;
  public notificationCount: number = 0;
  public authenticated: boolean = false;
  public unauthenticated: boolean = false;

  constructor(private logger: NGXLogger,
              private tracking: Tracking,
              public cache: SessionCache,
              private firebaseClient: FirebaseClient,
              private myService: MyService,
              private userService: UserService) {
    // Do nothing.
  }

  refreshAll(): Observable<any> {
    const sequence = forkJoin([
      this.refreshUser(),
      this.refreshNotificationCount(),
      this.cache.refreshAll()
    ]);

    sequence.subscribe(() => {
      this.logger.debug("** Session refreshed");
    });

    return sequence;
  }

  getUser(): Observable<User> {
    if (this.user) {
      return of(this.user);
    } else {
      return this.refreshUser();
    }
  }

  refreshUser(): Observable<User> {
    const sequence = this.firebaseClient.getAuthState().pipe(
      flatMap((firebaseUser) => firebaseUser ? this.userService.getUser(firebaseUser.uid) : of(null)),
      share()
    );

    sequence.subscribe((user) => {
      this.user = user;
      this.authenticated = (user != null);
      this.unauthenticated = !this.authenticated;
      this.tracking.identify(user);
    });

    return sequence;
  }

  refreshNotificationCount(): Observable<number> {
    return this.isAuthenticated().pipe(flatMap((authenticated) => {
      if (authenticated) {
        const sequence = this.myService.countMyNotifications();

        sequence.subscribe((notificationCount) => {
          this.notificationCount = notificationCount;
        });

        return sequence;
      } else {
        return of(0);
      }
    }));
  }

  isAuthenticated(): Observable<boolean> {
    return this.firebaseClient.getAuthState().pipe(
      map(firebaseUser => firebaseUser != null)
    );
  }

  signOut(): Observable<void> {
    const sequence = this.firebaseClient.signOut();

    sequence.subscribe(() => {
      this.user = null;
      this.authenticated = false;
      this.tracking.identify(null);
    });

    return sequence;
  }

}
