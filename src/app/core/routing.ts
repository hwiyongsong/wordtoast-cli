import { Injectable } from "@angular/core";
import { Hashtag } from "@app/entities/hashtag";
import { User } from "@app/entities/user";

@Injectable({
  providedIn: "root"
})
export class Routing {

  goHashtagDetailPage(hashtag: Hashtag) {
    return "/hashtags/" + hashtag.name;
  }

  goUserDetailPage(user: User) {
    return "/users/" + user.entityId;
  }

}
