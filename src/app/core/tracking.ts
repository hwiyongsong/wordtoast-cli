import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class Tracking {

  constructor() {

  }

  identify(identifier: any) {

  }

  trackPageImpression(pageKey: string, properties: any = {}) {

  }

  trackBlockImpression(blockKey: string, properties: any = {}) {

  }

  trackAction(actionKey: string, properties: any = {}) {

  }

}
