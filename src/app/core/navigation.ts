import { Injectable, NgZone } from "@angular/core";
import { NavigationExtras, Router } from "@angular/router";
import { NGXLogger } from "ngx-logger";

@Injectable({
  providedIn: "root"
})
export class Navigation {

  constructor(private logger: NGXLogger,
              private router: Router,
              private zone: NgZone) {
    // Do nothing.
  }

  goExplorePage(extras?: NavigationExtras) {
    this.go("explore", extras);
  }

  goFriendRequestDetailPage(friendRequestId: string, extras?: NavigationExtras) {
    this.go("friend-requests/" + friendRequestId, extras);
  }

  goGetStartedPage(extras?: NavigationExtras) {
    this.go("get-started", extras);
  }

  goHashtagDetailPage(hashtagId: string, extras?: NavigationExtras) {
    this.go("hashtags/" + hashtagId, extras);
  }

  goHomePage(extras?: NavigationExtras) {
    this.go("home", extras);
  }

  goInvitePage(extras?: NavigationExtras) {
    this.go("invite", extras);
  }

  goJoinPage(extras?: NavigationExtras) {
    this.go("join", extras);
  }

  goMyBookmarksPage(extras?: NavigationExtras) {
    this.go("me/bookmarks", extras);
  }

  goMyInterestsPage(extras?: NavigationExtras) {
    this.go("me/interests", extras);
  }

  goMyNotificationsPage(extras?: NavigationExtras) {
    this.go("me/notifications", extras);
  }

  goPostDetailPage(postId: string, extras?: NavigationExtras) {
    this.go("posts/" + postId, extras);
  }

  goMyPostsPage(extras?: NavigationExtras) {
    this.go("me/posts", extras);
  }

  goPrivacyPolicyPage(extras?: NavigationExtras) {
    this.go("privacy-policy", extras);
  }

  goMyProfileFormPage(extras?: NavigationExtras) {
    this.go("me/edit", extras);
  }

  goRootPage(extras?: NavigationExtras) {
    this.go("", extras);
  }

  goSearchPage(extras?: NavigationExtras) {
    this.go("search", extras);
  }

  goSettingsPage(extras?: NavigationExtras) {
    this.go("settings", extras);
  }

  goSignInPage(extras?: NavigationExtras) {
    this.go("sign-in", extras);
  }

  goTermsOfServicePage(extras?: NavigationExtras) {
    this.go("terms-of-service", extras);
  }

  goTopicDetailPage(topicId: string, extras?: NavigationExtras) {
    this.go("topics/" + topicId, extras);
  }

  goUserDetailPage(userId: string, extras?: NavigationExtras) {
    this.go("users/" + userId, extras);
  }

  goUserFollowersPage(userId: string, extras?: NavigationExtras) {
    this.go("users/" + userId + "/followers", extras);
  }

  goUserFollowingsPage(userId: string, extras?: NavigationExtras) {
    this.go("users/" + userId + "/followings", extras);
  }

  goUserFriendsPage(userId: string, extras?: NavigationExtras) {
    this.go("users/" + userId + "/friends", extras);
  }

  goUserStoriesPage(userId: string, extras?: NavigationExtras) {
    this.go("users/" + userId + "/stories", extras);
  }

  goWelcomePage(extras?: NavigationExtras) {
    this.go("welcome", extras);
  }

  go(url: string, extras?: NavigationExtras) {
    this.zone.run(() => {
      this.router.navigate([url], extras);
    });
  }

}
