import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { FriendRequestResolver } from "@app/resolvers/friend-request.resolver";
import { HashtagResolver } from "@app/resolvers/hashtag.resolver";
import { PostResolver } from "@app/resolvers/post.resolver";
import { TopicResolver } from "@app/resolvers/topic.resolver";
import { UserResolver } from "@app/resolvers/user.resolver";

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./pages/root/root.module").then(m => m.RootPageModule)
  },
  {
    path: "explore",
    loadChildren: () => import("./pages/explore/explore.module").then(m => m.ExplorePageModule)
  },
  {
    path: "friend-requests/:friendRequestId",
    loadChildren: () => import("./pages/friend-request-detail/friend-request-detail.module").then(m => m.FriendRequestDetailPageModule),
    resolve: {
      friendRequest: FriendRequestResolver
    }
  },
  {
    path: "get-started",
    loadChildren: () => import("./pages/get-started/get-started.module").then(m => m.GetStartedPageModule)
  },
  {
    path: "hashtags/:hashtagId",
    loadChildren: () => import("./pages/hashtag-detail/hashtag-detail.module").then(m => m.HashtagDetailPageModule),
    resolve: {
      hashtag: HashtagResolver
    }
  },
  {
    path: "home",
    loadChildren: () => import("./pages/home/home.module").then(m => m.HomePageModule)
  },
  {
    path: "invite",
    loadChildren: () => import("./pages/invite/invite.module").then(m => m.InvitePageModule)
  },
  {
    path: "join",
    loadChildren: () => import("./pages/join/join.module").then(m => m.JoinPageModule)
  },
  {
    path: "me/bookmarks",
    loadChildren: () => import("./pages/my-bookmarks/my-bookmarks.module").then(m => m.MyBookmarksPageModule)
  },
  {
    path: "me/interests",
    loadChildren: () => import("./pages/my-interests/my-interests.module").then(m => m.MyInterestsPageModule)
  },
  {
    path: "me/notifications",
    loadChildren: () => import("./pages/my-notifications/my-notifications.module").then(m => m.MyNotificationsPageModule)
  },
  {
    path: "me/posts",
    loadChildren: () => import("./pages/my-posts/my-posts.module").then(m => m.MyPostsPageModule)
  },
  {
    path: "me/edit",
    loadChildren: () => import("./pages/my-profile-edit/my-profile-edit.module").then(m => m.MyProfileEditPageModule)
  },
  {
    path: "posts/:postId",
    loadChildren: () => import("./pages/post-detail/post-detail.module").then(m => m.PostDetailPageModule),
    resolve: {
      post: PostResolver
    }
  },
  {
    path: "privacy-policy",
    loadChildren: () => import("./pages/privacy-policy/privacy-policy.module").then(m => m.PrivacyPolicyPageModule)
  },
  {
    path: "search",
    loadChildren: () => import("./pages/search/search.module").then(m => m.SearchPageModule)
  },
  {
    path: "settings",
    loadChildren: () => import("./pages/settings/settings.module").then(m => m.SettingsPageModule)
  },
  {
    path: "sign-in",
    loadChildren: () => import("./pages/sign-in/sign-in.module").then(m => m.SignInPageModule)
  },
  {
    path: "terms-of-service",
    loadChildren: () => import("./pages/terms-of-service/terms-of-service.module").then(m => m.TermsOfServicePageModule)
  },
  {
    path: "topics/:topicId",
    loadChildren: () => import("./pages/topic-detail/topic-detail.module").then(m => m.TopicDetailPageModule),
    resolve: {
      topic: TopicResolver
    }
  },
  {
    path: "users/:userId",
    loadChildren: () => import("./pages/user-detail/user-detail.module").then(m => m.UserDetailPageModule),
    resolve: {
      user: UserResolver
    }
  },
  {
    path: "users/:userId/followers",
    loadChildren: () => import("./pages/user-followers/user-followers.module").then(m => m.UserFollowersPageModule)
  },
  {
    path: "users/:userId/followings",
    loadChildren: () => import("./pages/user-followings/user-followings.module").then(m => m.UserFollowingsPageModule)
  },
  {
    path: "users/:userId/friends",
    loadChildren: () => import("./pages/user-friends/user-friends.module").then(m => m.UserFriendsPageModule)
  },
  {
    path: "users/:userId/stories",
    loadChildren: () => import("./pages/user-posts/user-posts.module").then(m => m.UserStoriesPageModule)
  },
  {
    path: "welcome",
    loadChildren: () => import("./pages/welcome/welcome.module").then(m => m.WelcomePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
