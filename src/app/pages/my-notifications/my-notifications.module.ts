import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { NotificationBlockModule } from "@app/blocks/notification/notification.module";
import { MyNotificationsPage } from "@app/pages/my-notifications/my-notifications.page";

const routes: Routes = [
  {
    path: "",
    component: MyNotificationsPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    NotificationBlockModule,
    BrandBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    MyNotificationsPage
  ]
})
export class MyNotificationsPageModule {
}
