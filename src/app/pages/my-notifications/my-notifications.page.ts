import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { AbstractPage } from "@app/pages/abstract.page";
import { MyService } from "@app/services/my.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-my-notifications-page",
  templateUrl: "./my-notifications.page.html",
  styleUrls: ["./my-notifications.page.scss"],
})
export class MyNotificationsPage extends AbstractPage implements OnInit {

  public notificationsFeed: Feed = new Feed();
  private notificationsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private session: Session,
              private tracking: Tracking,
              private myService: MyService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.tracking.trackPageImpression("app-my-notifications-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.markAllAsRead();
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<QueryResult> {
    const sequence = this.myService.queryMyNotifications(this.notificationsQuery.offset(0));

    sequence.subscribe((notifications) => {
      this.notificationsFeed.refresh(notifications);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const nextOffset = this.notificationsFeed.getNextOffset();
    const sequence = this.myService.queryMyBookmarks(this.notificationsQuery.offset(nextOffset));

    sequence.subscribe((notifications) => {
      this.notificationsFeed.append(notifications);

      if (event) {
        event.target.complete();
        event.target.disabled = this.notificationsFeed.eof;
      }
    });

    return sequence;
  }

  private markAllAsRead() {
    this.myService.markMyNotificationsAsRead().subscribe(() => {
      this.session.notificationCount = 0;
    });
  }

}
