import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { AuthenticationModalModule } from "@app/modals/authentication/authentication.module";
import { WelcomePage } from "@app/pages/welcome/welcome.page";

const routes: Routes = [
  {
    path: "",
    component: WelcomePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    BrandBlockModule,
    AuthenticationModalModule,
  ],
  declarations: [
    WelcomePage
  ]
})
export class WelcomePageModule {
}
