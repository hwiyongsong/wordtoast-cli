import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { AuthenticationModal } from "@app/modals/authentication/authentication.modal";
import { AbstractPage } from "@app/pages/abstract.page";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-welcome-page",
  templateUrl: "./welcome.page.html",
  styleUrls: ["./welcome.page.scss"],
})
export class WelcomePage extends AbstractPage implements OnInit {

  constructor(private chrome: Chrome,
              private modalController: ModalController) {
    super();
  }

  ngOnInit(): void {
    this.chrome.tracking.trackPageImpression("app-get-started-page");
  }

  async onGetStarted() {
    const modal = await this.modalController.create({
      component: AuthenticationModal,
      componentProps: {
        intent: "SIGN_UP_HOME"
      }
    });

    modal.onDidDismiss().then((event) => {
      if (event.data && event.data.status === "SUCCESS") {
        this.chrome.navigation.goHomePage();
      }
    });

    return await modal.present();
  }

  onDiscover() {
    this.chrome.navigation.goHomePage();
  }

}
