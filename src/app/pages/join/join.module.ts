import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { JoinPage } from "@app/pages/join/join.page";

const routes: Routes = [
  {
    path: "",
    component: JoinPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    JoinPage
  ]
})
export class JoinPageModule {
}
