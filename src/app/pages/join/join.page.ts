import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { EmailValidator } from "@app/validators/email-validator";

@Component({
  selector: "app-join-page",
  templateUrl: "./join.page.html",
  styleUrls: ["./join.page.scss"],
})
export class JoinPage implements OnInit {

  public joinForm: FormGroup;

  constructor(private chrome: Chrome,
              private formBuilder: FormBuilder,
              private navigation: Navigation,
              private tracking: Tracking,
              private route: ActivatedRoute,
              private session: Session,
              private firebaseClient: FirebaseClient,
              private userService: UserService) {
    this.joinForm = formBuilder.group({
      "displayName": ["", Validators.required],
      "email": ["", Validators.compose([Validators.required, EmailValidator.isValid])],
      "password": ["", Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-join-page");
  }

  onJoin() {
    if (this.joinForm.valid) {
      this.chrome.presentLoading().then(() => {
        const formData = this.joinForm.value;
        const displayName = formData.displayName;
        const email = formData.email;
        const password = formData.password;
        const referralCode = this.getReferralCode();

        this.firebaseClient.createUserWithEmailAndPassword(email, password).subscribe((firebaseCredential) => {
          const newUser = new User();
          newUser.displayName = displayName;
          newUser.email = email;
          newUser.firebaseUserId = firebaseCredential.user.uid;
          newUser.referredCode = referralCode;

          this.firebaseClient.updateUserDisplayName(displayName);

          this.userService.createUser(newUser).subscribe(() => {
            this.session.refreshAll().subscribe(() => {
              this.chrome.dismissLoading().then(() => {
                this.navigation.goHomePage();
              });
            });
          }, (error) => {
            this.chrome.dismissLoading();
            this.chrome.presentToast(error);
          });
        }, (error) => {
          this.chrome.dismissLoading();
          switch (error.code) {
            case "auth/email-already-in-use":
              this.chrome.presentToast("Email is already taken.");
              break;
            case "auth/weak-password":
              this.chrome.presentToast("Password is too weak.");
              break;
            default:
              this.chrome.presentToast("Unable to create an account: " + error.code);
          }
        });
      });
    }
  }

  private getReferralCode() {
    return this.route.snapshot.queryParams.ref;
  }

}
