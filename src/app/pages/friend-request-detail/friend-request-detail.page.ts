import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { FriendRequest } from "@app/entities/friend-request";
import { UserService } from "@app/services/user.service";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-friend-request-detail-page",
  templateUrl: "./friend-request-detail.page.html",
  styleUrls: ["./friend-request-detail.page.scss"],
})
export class FriendRequestDetailPage implements OnInit {

  public friendRequest: FriendRequest;

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              private tracking: Tracking,
              private alertController: AlertController,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-friend-request-detail-page");

    this.chrome.presentLoading().then(() => {
      const friendRequestId = this.getFriendRequestId();
      this.userService.getFriendRequestById(friendRequestId).subscribe((friendRequest) => {
        this.friendRequest = friendRequest;
        this.chrome.dismissLoading();
      });
    });
  }

  onUserDetail() {
    this.navigation.goUserDetailPage(this.friendRequest.createdBy.entityId);
  }

  onAcceptFriendRequest() {
    this.userService.acceptFriendRequest(this.friendRequest.entityId).subscribe((friendRequest) => {
      this.friendRequest = friendRequest;
    });
  }

  async onDeclineFriendRequest() {
    const alert = await this.alertController.create({
      header: "Delete friend request",
      message: "Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.userService.declineFriendRequest(this.friendRequest.entityId).subscribe((friendRequest) => {
              this.friendRequest = friendRequest;
            });
          }
        }
      ]
    });

    await alert.present();
  }

  private getFriendRequestId(): string {
    return this.route.snapshot.paramMap.get("friendRequestId");
  }

}
