import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { FriendRequestDetailPage } from "@app/pages/friend-request-detail/friend-request-detail.page";

const routes: Routes = [
  {
    path: "",
    component: FriendRequestDetailPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    UserAvatarBlockModule
  ],
  declarations: [
    FriendRequestDetailPage
  ]
})
export class FriendRequestDetailPageModule {
}
