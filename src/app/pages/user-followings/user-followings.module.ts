import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { UserBlockModule } from "@app/blocks/user/user.module";
import { UserFollowingsPage } from "@app/pages/user-followings/user-followings.page";

const routes: Routes = [
  {
    path: "",
    component: UserFollowingsPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    UserBlockModule
  ],
  declarations: [
    UserFollowingsPage
  ]
})
export class UserFollowingsPageModule {
}
