import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: "app-user-followings-page",
  templateUrl: "./user-followings.page.html",
  styleUrls: ["./user-followings.page.scss"],
})
export class UserFollowingsPage implements OnInit {

  public user: User;
  public followingsFeed: Feed = new Feed();
  public followingsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              private tracking: Tracking,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-user-followings-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<any> {
    const userId = this.getUserId();

    const sequence = forkJoin([
      this.userService.getUser(userId),
      this.userService.queryUserFollowings(userId, this.followingsQuery.offset(0))
    ]);

    sequence.subscribe(([user, followings]) => {
      this.user = user;
      this.followingsFeed.refresh(followings);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const userId = this.getUserId();
    const nextOffset = this.followingsFeed.getNextOffset();
    const sequence = this.userService.queryUsersFollowers(userId, this.followingsQuery.offset(nextOffset));

    sequence.subscribe((followings) => {
      this.followingsFeed.append(followings);

      if (event) {
        event.target.complete();
        event.target.disabled = this.followingsFeed.eof;
      }
    });

    return sequence;
  }

  private getUserId(): string {
    return this.route.snapshot.paramMap.get("userId");
  }

}
