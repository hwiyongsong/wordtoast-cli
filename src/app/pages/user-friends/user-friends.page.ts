import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: "app-user-friends-page",
  templateUrl: "./user-friends.page.html",
  styleUrls: ["./user-friends.page.scss"],
})
export class UserFriendsPage implements OnInit {

  public user: User;
  public friendsFeed: Feed = new Feed();
  public friendsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              private tracking: Tracking,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-user-friends-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<any> {
    const userId = this.getUserId();

    const sequence = forkJoin([
      this.userService.getUser(userId),
      this.userService.queryUserFriends(userId, this.friendsQuery.offset(0))
    ]);

    sequence.subscribe(([user, friends]) => {
      this.user = user;
      this.friendsFeed.refresh(friends);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const userId = this.getUserId();
    const nextOffset = this.friendsFeed.getNextOffset();
    const sequence = this.userService.queryUserFriends(userId, this.friendsQuery.offset(nextOffset));

    sequence.subscribe((followers) => {
      this.friendsFeed.append(followers);

      if (event) {
        event.target.complete();
        event.target.disabled = this.friendsFeed.eof;
      }
    });

    return sequence;
  }

  private getUserId(): string {
    return this.route.snapshot.paramMap.get("userId");
  }

}
