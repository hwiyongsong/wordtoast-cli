import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { UserBlockModule } from "@app/blocks/user/user.module";
import { UserFriendsPage } from "@app/pages/user-friends/user-friends.page";

const routes: Routes = [
  {
    path: "",
    component: UserFriendsPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    UserBlockModule
  ],
  declarations: [
    UserFriendsPage
  ]
})
export class UserFriendsPageModule {
}
