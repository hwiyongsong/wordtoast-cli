import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";

@Component({
  selector: "app-privacy-policy-page",
  templateUrl: "./privacy-policy.page.html",
  styleUrls: ["./privacy-policy.page.scss"],
})
export class PrivacyPolicyPage implements OnInit {

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private tracking: Tracking) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-privacy-policy-page");
  }

}
