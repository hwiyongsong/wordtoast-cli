import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { PrivacyPolicyPage } from "@app/pages/privacy-policy/privacy-policy.page";

const routes: Routes = [
  {
    path: "",
    component: PrivacyPolicyPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    BrandBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    PrivacyPolicyPage
  ]
})
export class PrivacyPolicyPageModule {
}
