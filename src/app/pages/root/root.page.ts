import { Component, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";

@Component({
  selector: "app-root-page",
  templateUrl: "./root.page.html",
  styleUrls: ["./root.page.scss"],
})
export class RootPage implements OnInit {

  constructor(private session: Session,
              private navigation: Navigation) {
    // Do nothing.
  }

  ngOnInit() {
    this.session.isAuthenticated().subscribe((authenticated) => {
      if (authenticated) {
        this.navigation.goHomePage();
      } else {
        this.navigation.goWelcomePage();
      }
    });
  }

}
