import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { RootPage } from "@app/pages/root/root.page";

const routes: Routes = [
  {
    path: "",
    component: RootPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
  ],
  declarations: [
    RootPage
  ]
})
export class RootPageModule {
}
