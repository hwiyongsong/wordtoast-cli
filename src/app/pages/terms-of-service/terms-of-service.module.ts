import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { TermsOfServicePage } from "@app/pages/terms-of-service/terms-of-service.page";

const routes: Routes = [
  {
    path: "",
    component: TermsOfServicePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    BrandBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    TermsOfServicePage
  ]
})
export class TermsOfServicePageModule {
}
