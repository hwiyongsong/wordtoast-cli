import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";

@Component({
  selector: "app-terms-of-service-page",
  templateUrl: "./terms-of-service.page.html",
  styleUrls: ["./terms-of-service.page.scss"],
})
export class TermsOfServicePage implements OnInit {

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private tracking: Tracking) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-terms-of-service-page");
  }

}
