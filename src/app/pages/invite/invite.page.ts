import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { ClipboardService } from "ngx-clipboard";

@Component({
  selector: "app-invite-page",
  templateUrl: "./invite.page.html",
  styleUrls: ["./invite.page.scss"],
})
export class InvitePage implements OnInit {

  public inviteUrl: string;

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private session: Session,
              private tracking: Tracking,
              private clipboardService: ClipboardService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-invite-page");
    this.inviteUrl = "http://wordtoast.com/get-started?ref=" + this.session.user.referralCode;
  }

  onCopyLink() {
    this.clipboardService.copy(this.inviteUrl);
    this.chrome.presentToast("Copied to clipboard");
  }

}
