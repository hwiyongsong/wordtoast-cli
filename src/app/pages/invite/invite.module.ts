import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { ComingSoonBlockModule } from "@app/blocks/coming-soon/coming-soon.module";
import { InvitePage } from "@app/pages/invite/invite.page";

const routes: Routes = [
  {
    path: "",
    component: InvitePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ComingSoonBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    InvitePage
  ]
})
export class InvitePageModule {
}
