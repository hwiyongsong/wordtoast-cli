import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { RequestParameter } from "@app/core/request-parameter";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Hashtag } from "@app/entities/hashtag";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { HashtagService } from "@app/services/hashtag.service";
import { PostService } from "@app/services/post.service";
import { ModalController } from "@ionic/angular";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: "app-hashtag-detail-page",
  templateUrl: "./hashtag-detail.page.html",
  styleUrls: ["./hashtag-detail.page.scss"],
})
export class HashtagDetailPage implements OnInit {

  public hashtag: Hashtag;
  public postsFeed: Feed = new Feed();
  private postsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              public session: Session,
              private tracking: Tracking,
              private modalController: ModalController,
              private hashtagService: HashtagService,
              private postService: PostService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-hashtag-detail-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<any> {
    const hashtagId = this.getHashtagId();

    const sequence = forkJoin([
      this.hashtagService.getHashtag(hashtagId),
      this.postService.queryPosts(this.postsQuery.put(RequestParameter.HASHTAG_ID, hashtagId).offset(0))
    ]);

    sequence.subscribe(([hashtag, posts]) => {
      this.hashtag = hashtag;
      this.postsFeed.refresh(posts);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const nextOffset = this.postsFeed.getNextOffset();
    const sequence = this.postService.queryPosts(this.postsQuery.offset(nextOffset));

    sequence.subscribe((posts) => {
      this.postsFeed.append(posts);

      if (event) {
        event.target.complete();
        event.target.disabled = this.postsFeed.eof;
      }
    });

    return sequence;
  }

  private getHashtagId(): string {
    return this.route.snapshot.paramMap.get("hashtagId");
  }

}
