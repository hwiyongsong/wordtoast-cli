import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { HashtagDetailPage } from "@app/pages/hashtag-detail/hashtag-detail.page";

const routes: Routes = [
  {
    path: "",
    component: HashtagDetailPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    EntityBlockModule
  ],
  declarations: [
    HashtagDetailPage
  ]
})
export class HashtagDetailPageModule {
}
