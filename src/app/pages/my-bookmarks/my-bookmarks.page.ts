import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { MyService } from "@app/services/my.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-my-bookmarks-page",
  templateUrl: "./my-bookmarks.page.html",
  styleUrls: ["./my-bookmarks.page.scss"],
})
export class MyBookmarksPage implements OnInit {

  public bookmarksFeed: Feed = new Feed();
  public bookmarksQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private tracking: Tracking,
              private myService: MyService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-my-bookmarks-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<QueryResult> {
    const sequence = this.myService.queryMyBookmarks(this.bookmarksQuery.offset(0));

    sequence.subscribe((bookmarks) => {
      this.bookmarksFeed.refresh(bookmarks);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const nextOffset = this.bookmarksFeed.getNextOffset();
    const sequence = this.myService.queryMyBookmarks(this.bookmarksQuery.offset(nextOffset));

    sequence.subscribe((bookmarks) => {
      this.bookmarksFeed.append(bookmarks);

      if (event) {
        event.target.complete();
        event.target.disabled = this.bookmarksFeed.eof;
      }
    });

    return sequence;
  }

}
