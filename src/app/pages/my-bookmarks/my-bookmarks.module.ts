import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PostBlockModule } from "@app/blocks/post/post.module";
import { MyBookmarksPage } from "@app/pages/my-bookmarks/my-bookmarks.page";

const routes: Routes = [
  {
    path: "",
    component: MyBookmarksPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    EntityBlockModule,
    PostBlockModule,
    ImageBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    MyBookmarksPage
  ]
})
export class MyBookmarksPageModule {
}
