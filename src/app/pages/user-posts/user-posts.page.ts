import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { UserService } from "@app/services/user.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-user-posts-page",
  templateUrl: "./user-posts.page.html",
  styleUrls: ["./user-posts.page.scss"],
})
export class UserPostsPage implements OnInit {

  public postFeed = new QueryResult();
  public postQuery = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              private tracking: Tracking,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-user-posts-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<QueryResult> {
    const userId = this.getUserId();
    const sequence = this.userService.queryUserPosts(userId, this.postQuery.offset(0));

    sequence.subscribe((postFeed) => {
      this.postFeed = postFeed;

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const userId = this.getUserId();
    const nextOffset = this.postFeed.getNextOffset();
    const sequence = this.userService.queryUserPosts(userId, this.postQuery.offset(nextOffset));

    sequence.subscribe((postFeed) => {
      this.postFeed.append(postFeed);

      if (event) {
        event.target.complete();
        event.target.disabled = postFeed.eof;
      }
    });

    return sequence;
  }

  private getUserId(): string {
    return this.route.snapshot.paramMap.get("userId");
  }

}
