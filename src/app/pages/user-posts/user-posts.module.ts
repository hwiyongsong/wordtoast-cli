import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { UserPostsPage } from "@app/pages/user-posts/user-posts.page";

const routes: Routes = [
  {
    path: "",
    component: UserPostsPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    EntityBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    UserPostsPage
  ]
})
export class UserStoriesPageModule {
}
