import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { PostPopoverBlockModule } from "@app/blocks/post-popover/post-popover.module";
import { PostBlockModule } from "@app/blocks/post/post.module";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";
import { MyPostsPage } from "@app/pages/my-posts/my-posts.page";

const routes: Routes = [
  {
    path: "",
    component: MyPostsPage
  }
];

@NgModule({
  declarations: [
    MyPostsPage
  ],
  entryComponents: [
    PostFormModal,
    PostPopoverBlock
  ],
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    FormsModule,
    ChromeHeaderBlockModule,
    ImageBlockModule,
    PostBlockModule,
    PostFormModalModule,
    PostPopoverBlockModule
  ]
})
export class MyPostsPageModule {
}
