import { Component, OnInit } from "@angular/core";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { Chrome } from "@app/core/chrome";
import { Event } from "@app/core/event";
import { EventBus } from "@app/core/event-bus";
import { RequestParameter } from "@app/core/request-parameter";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Post } from "@app/entities/post";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { AbstractPage } from "@app/pages/abstract.page";
import { MyService } from "@app/services/my.service";
import { ModalController, PopoverController } from "@ionic/angular";
import { Observable } from "rxjs";

@Component({
  selector: "app-my-posts-page",
  templateUrl: "./my-posts.page.html",
  styleUrls: ["./my-posts.page.scss"],
})
export class MyPostsPage extends AbstractPage implements OnInit {

  public postsFeed: Feed = new Feed();
  public postsQuery: Query = new Query();
  public publishStatus: string = "DRAFT";

  constructor(private chrome: Chrome,
              private eventBus: EventBus,
              private tracking: Tracking,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private myService: MyService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.tracking.trackPageImpression("app-my-posts-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });

    this.registerSubscription(this.eventBus.on(Event.POST_CREATED, (post) => {
      this.postsFeed.addToFirst(post);
    }));

    this.registerSubscription(this.eventBus.on(Event.POST_UPDATED, (post) => {
      this.postsFeed.update(post);
    }));

    this.registerSubscription(this.eventBus.on(Event.POST_REMOVED, (post) => {
      this.postsFeed.remove(post);
    }));
  }

  onRefresh(event?: any): Observable<QueryResult> {
    this.postsQuery.put(RequestParameter.PUBLISH_STATUS, this.publishStatus).offset(0);

    const sequence = this.myService.queryMyPosts(this.postsQuery);

    sequence.subscribe((posts) => {
      this.postsFeed.refresh(posts);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const nextOffset = this.postsFeed.getNextOffset();
    const sequence = this.myService.queryMyPosts(this.postsQuery.offset(nextOffset));

    sequence.subscribe((posts) => {
      this.postsFeed.append(posts);

      if (event) {
        event.target.complete();
        event.target.disabled = this.postsFeed.eof;
      }
    });

    return sequence;
  }

  onSegment(event: any) {
    this.onRefresh();
  }

  async onMore(post: Post, event: any) {
    const popover = await this.popoverController.create({
      component: PostPopoverBlock,
      componentProps: {
        post: post
      },
      event: event
    });

    return await popover.present();
  }

  async onEditPost(post: Post) {
    this.popoverController.dismiss();

    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        post: post
      },
      cssClass: "full"
    });

    return await modal.present();
  }

}
