import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { User } from "@app/entities/user";
import { ImageService } from "@app/services/image.service";
import { UserService } from "@app/services/user.service";

@Component({
  selector: "app-my-profile-form-page",
  templateUrl: "./my-profile-edit.page.html",
  styleUrls: ["./my-profile-edit.page.scss"],
})
export class MyProfileEditPage implements OnInit {

  public sessionUser: User;

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private session: Session,
              private tracking: Tracking,
              private imageService: ImageService,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-my-profile-form-page");

    this.session.getUser().subscribe((sessionUser) => {
      this.userService.getUser(this.session.user.entityId).subscribe((user) => {
        this.sessionUser = user;
      });
    });
  }

  async onSelectImage(event) {
    if (event.target.files && event.target.files.length > 0) {
      this.chrome.presentLoading().then(() => {
        const file = event.target.files[0];

        this.imageService.readAsDataUrl(file).subscribe((dataUrl) => {
          this.imageService.resize(dataUrl, 960, 960).subscribe((resizedDataUrl) => {
            this.imageService.createImageByDataUrl(resizedDataUrl).subscribe((image) => {
              this.chrome.dismissLoading();
              this.sessionUser.profileImage = image;
            });
          });
        });
      });
    }
  }

  onSave() {
    if (this.isSaveable()) {
      this.chrome.presentLoading().then(() => {
        this.userService.updateUser(this.sessionUser).subscribe(() => {
          this.session.refreshUser().subscribe((sessionUser) => {
            this.chrome.dismissLoading();
            this.sessionUser = sessionUser;
          });
        });
      });
    }
  }

  isSaveable() {
    return this.sessionUser.displayName && this.sessionUser.handle;
  }

}
