import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { MyProfileEditPage } from "@app/pages/my-profile-edit/my-profile-edit.page";

const routes: Routes = [
  {
    path: "",
    component: MyProfileEditPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    UserAvatarBlockModule,
    FormsModule
  ],
  declarations: [
    MyProfileEditPage
  ]
})
export class MyProfileEditPageModule {
}
