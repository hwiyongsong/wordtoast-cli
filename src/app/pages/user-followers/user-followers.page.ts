import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: "app-user-followers-page",
  templateUrl: "./user-followers.page.html",
  styleUrls: ["./user-followers.page.scss"],
})
export class UserFollowersPage implements OnInit {

  public user: User;
  public followersFeed: Feed = new Feed();
  public followersQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              private tracking: Tracking,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-user-followers-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<any> {
    const userId = this.getUserId();

    const sequence = forkJoin([
      this.userService.getUser(userId),
      this.userService.queryUsersFollowers(userId, this.followersQuery.offset(0))
    ]);

    sequence.subscribe(([user, followers]) => {
      this.user = user;
      this.followersFeed.refresh(followers);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const userId = this.getUserId();
    const nextOffset = this.followersFeed.getNextOffset();
    const sequence = this.userService.queryUsersFollowers(userId, this.followersQuery.offset(nextOffset));

    sequence.subscribe((followers) => {
      this.followersFeed.append(followers);

      if (event) {
        event.target.complete();
        event.target.disabled = this.followersFeed.eof;
      }
    });

    return sequence;
  }

  private getUserId(): string {
    return this.route.snapshot.paramMap.get("userId");
  }

}
