import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { UserBlockModule } from "@app/blocks/user/user.module";
import { UserFollowersPage } from "@app/pages/user-followers/user-followers.page";

const routes: Routes = [
  {
    path: "",
    component: UserFollowersPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    UserBlockModule
  ],
  declarations: [
    UserFollowersPage
  ]
})
export class UserFollowersPageModule {
}
