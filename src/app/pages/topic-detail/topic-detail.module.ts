import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";
import { TopicDetailPage } from "@app/pages/topic-detail/topic-detail.page";

const routes: Routes = [
  {
    path: "",
    component: TopicDetailPage
  }
];

@NgModule({
  declarations: [
    TopicDetailPage
  ],
  entryComponents: [
    PostFormModal
  ],
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    EntityBlockModule,
    PostFormModalModule
  ]
})
export class TopicDetailPageModule {
}
