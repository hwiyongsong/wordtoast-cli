import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { RequestParameter } from "@app/core/request-parameter";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Feed } from "@app/entities/feed";
import { PostType } from "@app/entities/post-type";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { Topic } from "@app/entities/topic";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostService } from "@app/services/post.service";
import { TopicService } from "@app/services/topic.service";
import { ModalController } from "@ionic/angular";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: "app-topic-detail-page",
  templateUrl: "./topic-detail.page.html",
  styleUrls: ["./topic-detail.page.scss"],
})
export class TopicDetailPage implements OnInit {

  public topic: Topic;
  public postsFeed: Feed = new Feed();
  private postsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private route: ActivatedRoute,
              public session: Session,
              private tracking: Tracking,
              private modalController: ModalController,
              private postService: PostService,
              private topicService: TopicService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-topic-detail-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<any> {
    const topicId = this.getTopicId();

    const sequence = forkJoin([
      this.topicService.getTopic(topicId),
      this.postService.queryPosts(this.postsQuery.put(RequestParameter.TOPIC_ID, topicId).offset(0))
    ]);

    sequence.subscribe(([topic, posts]) => {
      this.topic = topic;
      this.postsFeed.refresh(posts);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const topicId = this.getTopicId();
    const nextOffset = this.postsFeed.getNextOffset();
    const sequence = this.postService.queryPosts(this.postsQuery.offset(nextOffset));

    sequence.subscribe((posts) => {
      this.postsFeed.append(posts);

      if (event) {
        event.target.complete();
        event.target.disabled = this.postsFeed.eof;
      }
    });

    return sequence;
  }

  onCreateFollow() {
    this.topicService.createTopicFollow(this.topic).subscribe();
  }

  onRemoveFollow() {
    this.topicService.removeTopicFollow(this.topic).subscribe();
  }

  async onWrite() {
    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        postType: PostType.WRITING,
        topic: this.topic
      },
      cssClass: "full"
    });

    return await modal.present();
  }

  private getTopicId(): string {
    return this.route.snapshot.paramMap.get("topicId");
  }

}
