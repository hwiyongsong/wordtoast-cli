import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { SharePopoverBlock } from "@app/blocks/share-popover/share-popover.block";
import { Chrome } from "@app/core/chrome";
import { Event } from "@app/core/event";
import { RequestParameter } from "@app/core/request-parameter";
import { Feed } from "@app/entities/feed";
import { Post } from "@app/entities/post";
import { PostType } from "@app/entities/post-type";
import { Query } from "@app/entities/query";
import { PostCommentFormModal } from "@app/modals/post-comment-form/post-comment-form.modal";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { AbstractPage } from "@app/pages/abstract.page";
import { PostService } from "@app/services/post.service";
import { UserService } from "@app/services/user.service";
import { ModalController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-post-detail-page",
  templateUrl: "./post-detail.page.html",
  styleUrls: ["./post-detail.page.scss"],
})
export class PostDetailPage extends AbstractPage implements OnInit {

  public post: Post;
  public postCommentsFeed: Feed = new Feed();
  public responseFeed: Feed = new Feed();

  constructor(public chrome: Chrome,
              private route: ActivatedRoute,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private postService: PostService,
              private userService: UserService) {
    super();
  }

  ngOnInit() {
    this.post = this.route.snapshot.data.post;

    this.postService.queryPostComments(this.post, new Query().limit(3).offset(0)).subscribe((postComments) => {
      this.postCommentsFeed.refresh(postComments);
    });

    if (this.post.isPrompt()) {
      this.postService.queryPosts(new Query().put(RequestParameter.PROMPT_ID, this.post.entityId).offset(0)).subscribe((responseFeed) => {
        this.responseFeed.refresh(responseFeed);
      });
    }

    this.chrome.tracking.trackPageImpression("app-post-detail-page", {
      postId: this.post.entityId
    });

    this.registerSubscription(this.chrome.eventBus.on(Event.POST_UPDATED, (post) => {
      this.post = post;
    }));
  }

  ionViewWillEnter() {
    this.chrome.seo.setDefault();
    this.chrome.seo.setTitle(this.post.title);
    this.chrome.seo.setDescription(this.post.subtitle);

    if (this.post.coverImage) {
      this.chrome.seo.setHeroImageUrl(this.post.coverImage.sourceUrl);
    }
  }

  onUserDetail() {
    this.chrome.navigation.goUserDetailPage(this.post.createdBy.entityId);
  }

  onTopicDetail() {
    this.chrome.navigation.goTopicDetailPage(this.post.topic.entityId);
  }

  onHashtagDetail(hashtagId: string) {
    this.chrome.navigation.goHashtagDetailPage(hashtagId);
  }

  onPromptDetail() {
    this.chrome.navigation.goPostDetailPage(this.post.prompt.entityId);
  }

  onCreateFollow() {
    this.userService.createUserFollow(this.post.createdBy).subscribe();
  }

  onRemoveFollow() {
    this.userService.removeUserFollow(this.post.createdBy).subscribe();
  }

  onCreateReaction() {
    this.postService.createPostReaction(this.post).subscribe();
  }

  onRemoveReaction() {
    this.postService.removePostReaction(this.post).subscribe();
  }

  onCreateBookmark() {
    this.postService.createPostBookmark(this.post).subscribe();
  }

  onRemoveBookmark() {
    this.postService.removePostBookmark(this.post).subscribe();
  }

  onAuthenticate() {
    this.chrome.navigation.goWelcomePage();
  }

  async onShare(event) {
    const popover = await this.popoverController.create({
      component: SharePopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onMore(event) {
    const popover = await this.popoverController.create({
      component: PostPopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onWrite() {
    const post = new Post();
    post.postType = PostType.WRITING;

    if (this.post.isPrompt()) {
      post.prompt = this.post;
      post.topic = this.post.topic;
    }

    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        post: post
      },
      cssClass: "full"
    });

    return await modal.present();
  }

  async onCreateComment() {
    const modal = await this.modalController.create({
      component: PostCommentFormModal,
      componentProps: {
        post: this.post
      }
    });

    modal.onDidDismiss().then((event) => {
      if (event.data && event.data.postComment) {
        this.postCommentsFeed.addToFirst(event.data.postComment);
      }
    });

    return await modal.present();
  }

}
