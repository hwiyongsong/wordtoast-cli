import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { HashtagBlockModule } from "@app/blocks/hashtag/hashtag.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PostCommentBlockModule } from "@app/blocks/post-comment/post-comment.module";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { PostPopoverBlockModule } from "@app/blocks/post-popover/post-popover.module";
import { PostBlockModule } from "@app/blocks/post/post.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { PostCommentFormModal } from "@app/modals/post-comment-form/post-comment-form.modal";
import { PostCommentFormModalModule } from "@app/modals/post-comment-form/post-comment-form.module";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";
import { PostDetailPage } from "@app/pages/post-detail/post-detail.page";

const routes: Routes = [
  {
    path: "",
    component: PostDetailPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    ImageBlockModule,
    PostCommentBlockModule,
    PostCommentFormModalModule,
    PostFormModalModule,
    PostPopoverBlockModule,
    UserAvatarBlockModule,
    HashtagBlockModule,
    PostBlockModule
  ],
  exports: [],
  declarations: [
    PostDetailPage
  ],
  entryComponents: [
    PostCommentFormModal,
    PostFormModal,
    PostPopoverBlock
  ],
})
export class PostDetailPageModule {
}
