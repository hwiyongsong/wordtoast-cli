import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Feed } from "@app/entities/feed";
import { FriendRequest } from "@app/entities/friend-request";
import { Query } from "@app/entities/query";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { AlertController } from "@ionic/angular";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: "app-user-detail-page",
  templateUrl: "./user-detail.page.html",
  styleUrls: ["./user-detail.page.scss"],
})
export class UserDetailPage implements OnInit {

  public user: User;
  public postsFeed: Feed = new Feed();
  private postsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private route: ActivatedRoute,
              private alertController: AlertController,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    const userId = this.getUserId();

    this.chrome.tracking.trackPageImpression("app-user-detail-page", {
      "userId": userId
    });

    this.user = this.route.snapshot.data.user;

    this.userService.queryUserPosts(userId, this.postsQuery.offset(0)).subscribe((posts) => {
      this.postsFeed.refresh(posts);
    });
  }

  ionViewWillEnter() {
    this.chrome.seo.setDefault();
    this.chrome.seo.setTitle(this.user.displayName);

    if (this.user.headline) {
      this.chrome.seo.setDescription(this.user.headline);
    }

    if (this.user.profileImage) {
      this.chrome.seo.setHeroImageUrl(this.user.profileImage.sourceUrl);
    }
  }

  onRefresh(event?: any): Observable<any> {
    const userId = this.getUserId();

    const sequence = forkJoin([
      this.userService.getUser(userId),
      this.userService.queryUserPosts(userId, this.postsQuery.offset(0))
    ]);

    sequence.subscribe(([user, posts]) => {
      this.user = user;
      this.postsFeed.refresh(posts);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any) {
    const nextOffset = this.postsFeed.getNextOffset();
    const sequence = this.userService.queryUserPosts(this.getUserId(), this.postsQuery.offset(nextOffset));

    sequence.subscribe((posts) => {
      this.postsFeed.append(posts);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  async onCreateFriendRequest() {
    const alert = await this.alertController.create({
      header: "Send friend request to " + this.user.displayName,
      inputs: [
        {
          name: "notes",
          type: "textarea",
          placeholder: "Write a message..."
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "medium"
        }, {
          text: "Send",
          handler: (data) => {
            const friendRequest = new FriendRequest();
            friendRequest.user = this.user;
            friendRequest.notes = data.notes;

            this.userService.createFriendRequest(friendRequest).subscribe(() => {
              this.chrome.presentToast("Friend request sent to " + this.user.displayName);
            });
          }
        }
      ]
    });

    alert.present();
  }

  onCreateUserFollow() {
    this.userService.createUserFollow(this.user).subscribe();
  }

  onRemoveUserFollow() {
    this.userService.removeUserFollow(this.user).subscribe();
  }

  onFollowers() {
    this.chrome.navigation.goUserFollowersPage(this.user.entityId);
  }

  onFollowings() {
    this.chrome.navigation.goUserFollowingsPage(this.user.entityId);
  }

  onFriends() {
    this.chrome.navigation.goUserFriendsPage(this.user.entityId);
  }

  onEditMyProfile() {
    this.chrome.navigation.goMyProfileFormPage();
  }

  private getUserId(): string {
    return this.route.snapshot.params.userId;
  }

}
