import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { PostBlockModule } from "@app/blocks/post/post.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { UserDetailPage } from "@app/pages/user-detail/user-detail.page";

const routes: Routes = [
  {
    path: "",
    component: UserDetailPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    UserAvatarBlockModule,
    EntityBlockModule,
    BrandBlockModule,
    ChromeHeaderBlockModule,
    PostBlockModule
  ],
  declarations: [
    UserDetailPage
  ]
})
export class UserDetailPageModule {
}
