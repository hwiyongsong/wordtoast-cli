import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ComingSoonBlockModule } from "@app/blocks/coming-soon/coming-soon.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { ExplorePage } from "@app/pages/explore/explore.page";

const routes: Routes = [
  {
    path: "",
    component: ExplorePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ComingSoonBlockModule,
    EntityBlockModule
  ],
  declarations: [
    ExplorePage
  ]
})
export class ExplorePageModule {
}
