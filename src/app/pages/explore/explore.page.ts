import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Tracking } from "@app/core/tracking";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { ExploreService } from "@app/services/explore.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-explore-page",
  templateUrl: "./explore.page.html",
  styleUrls: ["./explore.page.scss"],
})
export class ExplorePage implements OnInit {

  public exploreFeed = new QueryResult();
  public exploreQuery = new Query();

  constructor(private chrome: Chrome,
              private tracking: Tracking,
              private exploreService: ExploreService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-explore-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<QueryResult> {
    const sequence = this.exploreService.queryExploreFeed(this.exploreQuery.offset(0));

    sequence.subscribe((exploreFeed) => {
      this.exploreFeed = exploreFeed;

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const nextOffset = this.exploreFeed.getNextOffset();
    const sequence = this.exploreService.queryExploreFeed(this.exploreQuery.offset(nextOffset));

    sequence.subscribe((exploreFeed) => {
      this.exploreFeed.append(exploreFeed);

      if (event) {
        event.target.complete();
        event.target.disabled = this.exploreFeed.eof;
      }
    });

    return sequence;
  }

}
