import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";

@Component({
  selector: "app-blank-page",
  templateUrl: "./blank.page.html",
  styleUrls: ["./blank.page.scss"],
})
export class BlankPage implements OnInit {

  constructor(private chrome: Chrome) {
    // Do nothing.
  }

  ngOnInit() {
    this.chrome.tracking.trackPageImpression("app-blank-page");
  }

}
