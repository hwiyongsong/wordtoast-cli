import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { BlankPage } from "@app/pages/blank/blank.page";

const routes: Routes = [
  {
    path: "",
    component: BlankPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule
  ],
  exports: [],
  declarations: [
    BlankPage
  ],
  entryComponents: []
})
export class BlankPageModule {
}
