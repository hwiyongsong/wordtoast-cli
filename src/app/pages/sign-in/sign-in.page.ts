import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";

@Component({
  selector: "app-sign-in-page",
  templateUrl: "./sign-in.page.html",
  styleUrls: ["./sign-in.page.scss"],
})
export class SignInPage implements OnInit {

  public signInForm: FormGroup;

  constructor(private chrome: Chrome,
              formBuilder: FormBuilder,
              private navigation: Navigation,
              private session: Session,
              private tracking: Tracking,
              private firebaseClient: FirebaseClient) {
    this.signInForm = formBuilder.group({
      "email": ["", Validators.required],
      "password": ["", Validators.required]
    });
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-sign-in-page");
  }

  onSignIn() {
    if (this.signInForm.valid) {
      this.chrome.presentLoading().then(() => {
        const formData = this.signInForm.value;
        const email = formData.email;
        const password = formData.password;

        this.firebaseClient.signInWithEmailAndPassword(email, password).subscribe(() => {
          this.session.refreshAll().subscribe(() => {
            this.chrome.dismissLoading().then(() => {
              this.navigation.goHomePage();
            });
          }, (error) => {
            this.chrome.dismissLoading();
            this.chrome.presentToast("Unable to sign in.");
          });
        }, (error) => {
          this.chrome.dismissLoading();
          this.chrome.presentToast("Unable to sign in.");
        });
      });
    }
  }

}
