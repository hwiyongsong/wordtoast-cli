import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { SignInPage } from "@app/pages/sign-in/sign-in.page";

const routes: Routes = [
  {
    path: "",
    component: SignInPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    SignInPage
  ]
})
export class SignInPageModule {
}
