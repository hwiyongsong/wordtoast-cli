import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { EntityBlockModule } from "@app/blocks/entity/entity.module";
import { NavItemBlockModule } from "@app/blocks/nav-item/nav-item.module";
import { PopularPromptsBlockModule } from "@app/blocks/popular-prompts/popular-prompts.module";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";
import { HomePage } from "@app/pages/home/home.page";

const routes: Routes = [
  {
    path: "",
    component: HomePage
  }
];

@NgModule({
  declarations: [
    HomePage
  ],
  entryComponents: [
    PostFormModal
  ],
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    EntityBlockModule,
    NavItemBlockModule,
    PopularPromptsBlockModule,
    PostFormModalModule
  ]
})
export class HomePageModule {
}
