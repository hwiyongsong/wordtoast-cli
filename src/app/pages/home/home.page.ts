import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Chrome } from "@app/core/chrome";
import { Event } from "@app/core/event";
import { RequestParameter } from "@app/core/request-parameter";
import { Feed } from "@app/entities/feed";
import { PostType } from "@app/entities/post-type";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { AbstractPage } from "@app/pages/abstract.page";
import { HomeService } from "@app/services/home.service";
import { IonContent, ModalController } from "@ionic/angular";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-home-page",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage extends AbstractPage implements OnInit {

  @ViewChild("homePageContent", {static: true}) content: IonContent;

  public homeSegment: string;
  public homeFeed = new Feed();
  private homeQuery = new Query();

  constructor(public chrome: Chrome,
              private route: ActivatedRoute,
              private modalController: ModalController,
              private homeService: HomeService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.chrome.tracking.trackPageImpression("app-home-page");

    this.chrome.presentLoading().then(() => {
      this.deriveHomeSegment().subscribe(() => {
        this.onRefresh().subscribe(() => {
          this.chrome.dismissLoading();
        });
      });
    });

    this.registerSubscription(this.chrome.eventBus.on(Event.POST_CREATED, (post) => {
      if (post.isPublished()) {
        this.homeFeed.addToFirst(post);
      }
    }));

    this.registerSubscription(this.chrome.eventBus.on(Event.POST_UPDATED, (post) => {
      if (post.isPublished()) {
        this.homeFeed.update(post);
      }
    }));

    this.registerSubscription(this.chrome.eventBus.on(Event.POST_REMOVED, (post) => {
      if (post.isPublished()) {
        this.homeFeed.remove(post);
      }
    }));
  }

  ionViewWillEnter() {
    this.chrome.seo.setDefault();
  }

  onRefresh(event?: any): Observable<QueryResult> {
    const sequence = this.onLoadSegment(this.homeSegment);

    sequence.subscribe(() => {
      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onInfiniteScroll(event?: any) {
    const nextOffset = this.homeFeed.getNextOffset();
    const sequence = this.homeService.queryHomeFeed(this.homeQuery.offset(nextOffset));

    sequence.subscribe((entities) => {
      this.homeFeed.append(entities);

      if (event) {
        event.target.complete();
      }
    });

    return sequence;
  }

  onLoadSegment(homeSegment: string): Observable<QueryResult> {
    this.homeSegment = homeSegment;
    this.homeQuery.put(RequestParameter.SEGMENT, this.homeSegment);
    this.homeFeed.clear();

    const sequence = this.homeService.queryHomeFeed(this.homeQuery.offset(0));

    sequence.subscribe((entities) => {
      this.homeFeed.refresh(entities);
    });

    return sequence;
  }

  async onWrite() {
    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        postType: PostType.WRITING
      },
      cssClass: "full"
    });

    return await modal.present();
  }

  private deriveHomeSegment(): Observable<string> {
    if (this.homeSegment) {
      return of(this.homeSegment);
    }

    return this.chrome.session.isAuthenticated().pipe(map((authenticated) => {
      if (authenticated) {
        this.homeSegment = this.route.snapshot.queryParams.segment || "POPULAR";
      } else {
        this.homeSegment = "POPULAR";
      }

      return this.homeSegment;
    }));
  }

  private scrollToTop() {
    const DELAY_BEFORE_SCROLLING = 400;
    const SCROLLING_DURATION = 400;

    setTimeout(() => {
      this.content.scrollToTop(SCROLLING_DURATION);
    }, DELAY_BEFORE_SCROLLING);
  }

}
