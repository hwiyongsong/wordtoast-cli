import { OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

export abstract class AbstractPage implements OnInit, OnDestroy {

  private subscriptions: Array<Subscription> = [];

  ngOnInit() {
    // Do nothing.
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  registerSubscription(subscription: Subscription) {
    this.subscriptions.push(subscription);
  }

}
