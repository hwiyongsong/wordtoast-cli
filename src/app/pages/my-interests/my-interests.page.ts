import { Component, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Query } from "@app/entities/query";
import { QueryResult } from "@app/entities/query-result";
import { TopicService } from "@app/services/topic.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-my-interests-page",
  templateUrl: "./my-interests.page.html",
  styleUrls: ["./my-interests.page.scss"],
})
export class MyInterestsPage implements OnInit {

  public topicsFeed: QueryResult = new QueryResult();
  public topicsQuery: Query = new Query();

  constructor(private chrome: Chrome,
              private navigation: Navigation,
              private tracking: Tracking,
              private topicService: TopicService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-user-followings-page");

    this.chrome.presentLoading().then(() => {
      this.onRefresh().subscribe(() => {
        this.chrome.dismissLoading();
      });
    });
  }

  onRefresh(event?: any): Observable<any> {
    const sequence = this.topicService.queryTopics(this.topicsQuery.offset(0));

    sequence.subscribe((topicsFeed) => {
      this.topicsFeed.refresh(topicsFeed);
    });

    return sequence;
  }

  onInfiniteScroll(event?: any): Observable<QueryResult> {
    const nextOffset = this.topicsFeed.getNextOffset();
    const sequence = this.topicService.queryTopics(this.topicsQuery.offset(nextOffset));

    sequence.subscribe((topicsFeed) => {
      this.topicsFeed.append(topicsFeed);

      if (event) {
        event.target.complete();
        event.target.disabled = topicsFeed.eof;
      }
    });

    return sequence;
  }

}
