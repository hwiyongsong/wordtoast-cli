import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { TopicBlockModule } from "@app/blocks/topic/topic.module";
import { MyInterestsPage } from "@app/pages/my-interests/my-interests.page";

const routes: Routes = [
  {
    path: "",
    component: MyInterestsPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    ChromeHeaderBlockModule,
    TopicBlockModule
  ],
  declarations: [
    MyInterestsPage
  ]
})
export class MyInterestsPageModule {
}
