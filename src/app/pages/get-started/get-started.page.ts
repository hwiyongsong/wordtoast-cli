import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { NGXLogger } from "ngx-logger";

@Component({
  selector: "app-get-started-page",
  templateUrl: "./get-started.page.html",
  styleUrls: ["./get-started.page.scss"],
})
export class GetStartedPage implements OnInit {

  constructor(private logger: NGXLogger,
              private navigation: Navigation,
              private route: ActivatedRoute,
              private session: Session,
              private tracking: Tracking,
              private firebaseClient: FirebaseClient,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-get-started-page");
  }

  onContinueWithFacebook() {
    this.firebaseClient.signInWithFacebook().subscribe((firebaseUserCredential) => {
      const firebaseUser = firebaseUserCredential.user;
      const firebaseUserId = firebaseUser.uid;

      this.userService.getUser(firebaseUserId).subscribe((user) => {
        this.session.user = user;
        this.navigation.goHomePage();
      }, (notFound) => {
        const displayName = firebaseUser.displayName;
        const email = firebaseUser.email;
        const facebookUserId = firebaseUser.providerData[0].uid;

        const user = new User();
        user.displayName = displayName;
        user.email = email;
        user.firebaseUserId = firebaseUserId;
        user.facebookUserId = facebookUserId;

        this.userService.createUser(user).subscribe((newUser) => {
          this.session.user = newUser;
          this.navigation.goHomePage();
        });
      });
    }, (error) => {
      this.logger.error(error);
    });
  }

  onJoin() {
    this.navigation.goJoinPage({
      queryParams: {
        ref: this.getReferralCode()
      }
    });
  }

  onSignIn() {
    this.navigation.goSignInPage();
  }

  onTermsOfService() {
    this.navigation.goTermsOfServicePage();
  }

  onPrivacyPolicy() {
    this.navigation.goPrivacyPolicyPage();
  }

  private getReferralCode() {
    return this.route.snapshot.queryParams.ref;
  }

}
