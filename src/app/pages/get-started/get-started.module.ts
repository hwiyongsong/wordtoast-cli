import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { GetStartedPage } from "@app/pages/get-started/get-started.page";

const routes: Routes = [
  {
    path: "",
    component: GetStartedPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    BrandBlockModule,
    ChromeHeaderBlockModule
  ],
  declarations: [
    GetStartedPage
  ]
})
export class GetStartedPageModule {
}
