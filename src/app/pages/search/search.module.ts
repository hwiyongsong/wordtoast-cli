import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlockModule } from "@app/blocks/chrome-header/chrome-header.module";
import { ComingSoonBlockModule } from "@app/blocks/coming-soon/coming-soon.module";
import { PostBlockModule } from "@app/blocks/post/post.module";
import { UserBlockModule } from "@app/blocks/user/user.module";
import { SearchPage } from "@app/pages/search/search.page";

const routes: Routes = [
  {
    path: "",
    component: SearchPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    AppCommonModule,
    BrandBlockModule,
    ComingSoonBlockModule,
    ChromeHeaderBlockModule,
    FormsModule,
    UserBlockModule,
    PostBlockModule
  ],
  declarations: [
    SearchPage
  ]
})
export class SearchPageModule {
}
