import { AfterViewInit, Component, OnInit, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Tracking } from "@app/core/tracking";
import { SearchResult } from "@app/entities/search-result";
import { SearchService } from "@app/services/search.service";
import { ModalController } from "@ionic/angular";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: "app-search-page",
  templateUrl: "./search.page.html",
  styleUrls: ["./search.page.scss"],
})
export class SearchPage implements OnInit, AfterViewInit {

  @ViewChild("searchInputRef", {static: false}) searchInputRef;
  public searchInput = new FormControl("");
  public searchResult: SearchResult = new SearchResult();

  constructor(private tracking: Tracking,
              private modalController: ModalController,
              private searchService: SearchService) {
    this.searchInput
        .valueChanges
        .pipe(debounceTime(400))
        .subscribe(input => this.onInput(input));
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-search-page");
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.searchInputRef.setFocus(true);
    }, 400);
  }

  onInput(input: string) {
    if (input) {
      this.searchService.search(input).subscribe((searchResult) => {
        if (this.searchInput.value === searchResult.input) {
          this.searchResult = searchResult;
        }
      });
    } else {
      this.searchResult.clear();
    }
  }

}
