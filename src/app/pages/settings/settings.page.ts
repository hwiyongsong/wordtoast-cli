import { Component, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";

@Component({
  selector: "app-settings-page",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"],
})
export class SettingsPage implements OnInit {

  constructor(private navigation: Navigation,
              private session: Session,
              private tracking: Tracking) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackPageImpression("app-settings-page");
  }

  onSignOut() {
    this.session.signOut().subscribe(() => {
      this.navigation.goWelcomePage();
    });
  }

}
