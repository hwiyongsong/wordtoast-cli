import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostSettingsModal } from "@app/modals/post-settings/post-settings.modal";
import { PostSettingsModalModule } from "@app/modals/post-settings/post-settings.module";
import { TopicSelectorPopoverModule } from "@app/popovers/topic-selector/topic-selector.module";
import { TopicSelectorPopover } from "@app/popovers/topic-selector/topic-selector.popover";
import { VisibilitySelectorPopoverModule } from "@app/popovers/visibility-selector/visibility-selector.module";
import { VisibilitySelectorPopover } from "@app/popovers/visibility-selector/visibility-selector.popover";

@NgModule({
  declarations: [
    PostFormModal
  ],
  entryComponents: [
    PostSettingsModal,
    TopicSelectorPopover,
    VisibilitySelectorPopover
  ],
  imports: [
    AppCommonModule,
    FormsModule,
    ImageBlockModule,
    PostSettingsModalModule,
    TopicSelectorPopoverModule,
    UserAvatarBlockModule,
    VisibilitySelectorPopoverModule
  ]
})
export class PostFormModalModule {
}
