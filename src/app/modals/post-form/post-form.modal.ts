import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { PostType } from "@app/entities/post-type";
import { PublishStatus } from "@app/entities/publish-status";
import { Topic } from "@app/entities/topic";
import { PostSettingsModal } from "@app/modals/post-settings/post-settings.modal";
import { TopicSelectorPopover } from "@app/popovers/topic-selector/topic-selector.popover";
import { VisibilitySelectorPopover } from "@app/popovers/visibility-selector/visibility-selector.popover";
import { ImageService } from "@app/services/image.service";
import { PostService } from "@app/services/post.service";
import { EntityUtils } from "@app/utils/entity-utils";

import { AlertController, ModalController, PopoverController } from "@ionic/angular";
import Quill from "quill";

@Component({
  selector: "app-post-form-modal",
  templateUrl: "./post-form.modal.html",
  styleUrls: ["./post-form.modal.scss"],
})
export class PostFormModal implements OnInit, AfterViewInit, OnDestroy {

  private static readonly AUTOSAVE_INTERVAL_IN_MILLISECONDS = 10000;

  @ViewChild("titleInput", {static: true}) titleInput: any;

  @Input() post: Post = new Post();
  @Input() postType: PostType = PostType.WRITING;
  @Input() topic: Topic;

  public titlePlaceholder: string = "Title...";
  public bodyPlaceholder: string = "What's on your mind?";
  private quillEditor: any;
  private autosaveWorker: any;
  private dirty: boolean = false;

  constructor(private chrome: Chrome,
              public session: Session,
              private tracking: Tracking,
              private alertController: AlertController,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private imageService: ImageService,
              private postService: PostService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackPageImpression("app-post-form-modal", {
      postId: this.post.entityId
    });

    this.topic = this.post.topic || this.topic;

    if (this.post.isPrototype()) {
      this.post.postType = this.postType;
      this.post.topic = this.topic;
    } else {
      this.postType = this.post.postType;
      this.post.topic = this.topic;
    }

    console.log("** this.topic = " + this.post.topic);

    if (this.post.isWriting()) {
      this.titlePlaceholder = "Title...";
      this.bodyPlaceholder = "What's on your mind?";
    }

    if (this.post.isPrompt()) {
      this.titlePlaceholder = "Title of writing prompt...";
      this.bodyPlaceholder = "What should people write about?";
    }
  }

  ngAfterViewInit(): void {
    this.initTitleFocus();
    this.initQuillEditor();
    this.initAutosaveWorker();
  }

  ngOnDestroy(): void {
    this.destroyAutosaveWorker();
  }

  async onClose() {
    await this.modalController.dismiss();
  }

  onPublish() {
    if (this.isPostable()) {
      this.post.publishStatus = PublishStatus.PUBLISHED;
      const sequence = this.post.isPrototype() ? this.postService.createPost(this.post) : this.postService.updatePost(this.post);

      sequence.subscribe((post) => {
        this.modalController.dismiss({
          post: post
        });
      });
    }
  }

  onUpdate() {
    if (this.isPostable()) {
      this.postService.updatePost(this.post).subscribe((post) => {
        this.modalController.dismiss({
          post: post
        });
      });
    }
  }

  onUploadCoverImage() {
    this.uploadImage((image) => {
      this.post.coverImage = image;
      this.onDirty();
    });
  }

  async onRemoveCoverImage() {
    const alert = await this.alertController.create({
      header: "Remove cover photo",
      message: "Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.post.coverImage = null;
            this.onDirty();
          }
        }
      ]
    });

    await alert.present();
  }

  async onSelectTopic(event) {
    const popover = await this.popoverController.create({
      component: TopicSelectorPopover,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onSelectVisibility(event) {
    const popover = await this.popoverController.create({
      component: VisibilitySelectorPopover,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onSettings() {
    const modal = await this.modalController.create({
      component: PostSettingsModal,
      componentProps: {
        post: this.post
      }
    });

    return await modal.present();
  }

  onDirty() {
    this.dirty = true;
  }

  isPostable() {
    const containsTitle = this.post.title && this.post.title.length > 0;
    const containsBody = this.post.body && this.post.body.length > 0;
    return containsTitle && containsBody;
  }

  compareWith = (object1, object2) => EntityUtils.equals(object1, object2);

  private initTitleFocus() {
    setTimeout(() => {
      this.titleInput.setFocus(true);
    }, 400);
  }

  private initQuillEditor() {
    this.quillEditor = new Quill("#quill-editor", {
      modules: {
        toolbar: {
          container: "#quill-toolbar",
          handlers: {
            image: () => this.uploadImageToQuill()
          }
        }
      },
      theme: "snow",
      placeholder: this.bodyPlaceholder
    });

    this.quillEditor.clipboard.dangerouslyPasteHTML(0, this.post.body);

    this.quillEditor.on("text-change", () => {
      this.post.body = this.quillEditor.root.innerHTML;
      this.onDirty();
    });
  }

  private uploadImageToQuill() {
    this.uploadImage((image) => {
      const range = this.quillEditor.getSelection();
      this.quillEditor.insertEmbed(range.index, "image", image.sourceUrl);
      this.quillEditor.setSelection(range.index + 1, Quill.sources.SILENT);
      this.onDirty();
    });
  }

  private uploadImage(callback) {
    const fileInput = document.createElement("input");
    fileInput.setAttribute("type", "file");
    fileInput.setAttribute("accept", "image/*");
    fileInput.addEventListener("change", () => {
      const file = fileInput.files[0];

      if (/^image\//.test(file.type)) {
        this.chrome.presentLoading().then(() => {
          this.imageService.readAsDataUrl(file).subscribe((dataUrl) => {
            this.imageService.resize(dataUrl, 960, 960).subscribe((resizedDataUrl) => {
              const sequence = this.imageService.createImageByDataUrl(resizedDataUrl);
              this.imageService.createImageByDataUrl(resizedDataUrl).subscribe((image) => {
                this.chrome.dismissLoading();

                if (callback) {
                  callback(image);
                }
              });
            });
          });
        });
      }
    });

    fileInput.click();
  }

  private saveDraft() {
    if (this.dirty && this.post.isDraft() && this.isPostable()) {
      const sequence = this.post.isPrototype() ? this.postService.createPost(this.post) : this.postService.updatePost(this.post);

      sequence.subscribe((post) => {
        this.post.entityUrn = post.entityUrn;
        this.post.entityId = post.entityId;
        this.post.entityType = post.entityType;
        this.post.updatedAt = post.updatedAt;
        this.dirty = false;
      });
    }
  }

  private initAutosaveWorker() {
    this.autosaveWorker = setInterval(() => {
      this.saveDraft();
    }, PostFormModal.AUTOSAVE_INTERVAL_IN_MILLISECONDS);
  }

  private destroyAutosaveWorker() {
    if (this.autosaveWorker) {
      clearInterval(this.autosaveWorker);
    }
  }

}
