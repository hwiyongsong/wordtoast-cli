import { NgModule } from "@angular/core";
import { BlankModal } from "@app/modals/blank/blank.modal";
import { AppCommonModule } from "@app/app-common.module";

@NgModule({
  imports: [
    AppCommonModule
  ],
  exports: [],
  declarations: [
    BlankModal
  ],
  entryComponents: [
    BlankModal
  ]
})
export class BlankModalModule {
}
