import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-blank-modal",
  templateUrl: "./blank.modal.html",
  styleUrls: ["./blank.modal.scss"],
})
export class BlankModal implements OnInit {

  constructor(private modalController: ModalController) {
    // Do nothing.
  }

  ngOnInit(): void {

  }

  async onClose() {
    await this.modalController.dismiss();
  }

}
