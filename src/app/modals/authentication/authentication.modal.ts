import { Component, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Chrome } from "@app/core/chrome";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { EmailValidator } from "@app/validators/email-validator";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-authentication-modal",
  templateUrl: "./authentication.modal.html",
  styleUrls: ["./authentication.modal.scss"],
})
export class AuthenticationModal {

  @Input() intent: string = "SIGN_UP_HOME";
  @Input() referralCode: string;

  public signUpForm: FormGroup;
  public signInForm: FormGroup;
  public forgotPasswordForm: FormGroup;

  constructor(private chrome: Chrome,
              formBuilder: FormBuilder,
              private modalController: ModalController,
              private firebaseClient: FirebaseClient,
              private userService: UserService) {
    this.signUpForm = formBuilder.group({
      "displayName": ["", Validators.required],
      "email": ["", Validators.compose([Validators.required, EmailValidator.isValid])],
      "password": ["", Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    this.signInForm = formBuilder.group({
      "email": ["", Validators.required],
      "password": ["", Validators.required]
    });

    this.forgotPasswordForm = formBuilder.group({
      "email": ["", Validators.required]
    });
  }

  async onClose() {
    await this.modalController.dismiss({
      "status": "CLOSE"
    });
  }

  onSignUpHome() {
    this.intent = "SIGN_UP_HOME";
  }

  onSignInHome() {
    this.intent = "SIGN_IN_HOME";
  }

  onSignUpForm() {
    this.intent = "SIGN_UP_FORM";
  }

  onSignInForm() {
    this.intent = "SIGN_IN_FORM";
  }

  onForgotPasswordForm() {
    this.intent = "FORGOT_PASSWORD_FORM";
  }

  onSignUpSubmit() {
    if (this.signUpForm.valid) {
      this.chrome.presentLoading().then(() => {
        const formData = this.signUpForm.value;
        const displayName = formData.displayName;
        const email = formData.email;
        const password = formData.password;

        this.firebaseClient.createUserWithEmailAndPassword(email, password).subscribe((firebaseCredential) => {
          const newUser = new User();
          newUser.displayName = displayName;
          newUser.email = email;
          newUser.firebaseUserId = firebaseCredential.user.uid;
          newUser.referredCode = this.referralCode;

          this.firebaseClient.updateUserDisplayName(displayName);

          this.userService.createUser(newUser).subscribe(() => {
            this.chrome.session.refreshAll().subscribe(() => {
              this.chrome.dismissLoading().then(() => {
                this.modalController.dismiss({
                  "status": "SUCCESS"
                });
              });
            });
          }, (error) => {
            this.chrome.dismissLoading();
            this.chrome.presentToast(error);

            this.modalController.dismiss({
              "status": "ERROR"
            });
          });
        }, (error) => {
          this.chrome.dismissLoading();

          switch (error.code) {
            case "auth/email-already-in-use":
              this.chrome.presentToast("Email is already taken.");
              break;
            case "auth/weak-password":
              this.chrome.presentToast("Password is too weak.");
              break;
            default:
              this.chrome.presentToast("Unable to create an account: " + error.code);
          }

          this.modalController.dismiss({
            "status": "ERROR"
          });
        });
      });
    }
  }

  onSignInSubmit() {
    if (this.signInForm.valid) {
      this.chrome.presentLoading().then(() => {
        const formData = this.signInForm.value;
        const email = formData.email;
        const password = formData.password;

        this.firebaseClient.signInWithEmailAndPassword(email, password).subscribe(() => {
          this.chrome.session.refreshAll().subscribe(() => {
            this.chrome.dismissLoading().then(() => {
              this.modalController.dismiss({
                "status": "SUCCESS"
              });
            });
          }, (error) => {
            this.chrome.dismissLoading();
            this.chrome.presentToast("Unable to sign in: " + error);
            this.modalController.dismiss({
              "status": "ERROR"
            });
          });
        }, (error) => {
          this.chrome.dismissLoading();
          this.chrome.presentToast("Unable to sign in: " + error);
          this.modalController.dismiss({
            "status": "ERROR"
          });
        });
      });
    }
  }

  onContinueWithFacebook() {
    this.firebaseClient.signInWithFacebook().subscribe((firebaseUserCredential) => {
      const firebaseUser = firebaseUserCredential.user;
      const firebaseUserId = firebaseUser.uid;

      this.userService.getUser(firebaseUserId).subscribe((user) => {
        this.chrome.session.refreshAll().subscribe(() => {
          this.modalController.dismiss({
            "status": "SUCCESS"
          });
        });
      }, (notFound) => {
        const displayName = firebaseUser.displayName;
        const email = firebaseUser.email;
        const facebookUserId = firebaseUser.providerData[0].uid;

        const user = new User();
        user.displayName = displayName;
        user.email = email;
        user.firebaseUserId = firebaseUserId;
        user.facebookUserId = facebookUserId;

        this.userService.createUser(user).subscribe((newUser) => {
          this.chrome.session.refreshAll().subscribe(() => {
            this.modalController.dismiss({
              "status": "SUCCESS"
            });
          });
        });
      });
    }, (error) => {
      this.chrome.logger.error(error);

      this.modalController.dismiss({
        "status": "ERROR"
      });
    });
  }

  onForgotPasswordSubmit() {
    if (this.forgotPasswordForm.valid) {
      this.chrome.presentLoading().then(() => {
        const formData = this.forgotPasswordForm.value;
        const email = formData.email;

        this.firebaseClient.sendPasswordResetEmail(email).subscribe(() => {
          this.chrome.dismissLoading();
          this.chrome.presentToast("Sent password reset email.");
        }, (error) => {
          this.chrome.dismissLoading();
          this.chrome.presentToast("Unable to send password reset email: " + error);
        });
      });
    }
  }

}
