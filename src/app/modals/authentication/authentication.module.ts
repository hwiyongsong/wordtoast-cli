import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { AuthenticationModal } from "@app/modals/authentication/authentication.modal";

@NgModule({
  imports: [
    AppCommonModule
  ],
  exports: [],
  declarations: [
    AuthenticationModal
  ],
  entryComponents: [
    AuthenticationModal
  ]
})
export class AuthenticationModalModule {
}
