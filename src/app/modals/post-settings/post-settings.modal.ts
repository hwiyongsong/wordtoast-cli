import { Component, Input, OnInit } from "@angular/core";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { Visibility } from "@app/entities/visibility";
import { EntityUtils } from "@app/utils/entity-utils";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-post-settings-modal",
  templateUrl: "./post-settings.modal.html",
  styleUrls: ["./post-settings.modal.scss"],
})
export class PostSettingsModal implements OnInit {

  @Input() post: Post = new Post();
  public visibilityEnum = Visibility;

  constructor(public session: Session,
              private tracking: Tracking,
              private modalController: ModalController) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackPageImpression("app-post-settings-modal");
  }

  async onClose() {
    await this.modalController.dismiss();
  }

  compareWithEntities = (object1, object2) => EntityUtils.equals(object1, object2);

  compareWithEnums = (object1, object2) => object1 === object2;

}
