import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppCommonModule } from "@app/app-common.module";
import { PostSettingsModal } from "@app/modals/post-settings/post-settings.modal";

@NgModule({
  imports: [
    AppCommonModule,
    FormsModule
  ],
  declarations: [
    PostSettingsModal
  ],
  entryComponents: [
    PostSettingsModal
  ]
})
export class PostSettingsModalModule {
}
