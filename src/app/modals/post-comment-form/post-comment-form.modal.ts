import { AfterViewInit, Component, Input, OnInit, ViewChild } from "@angular/core";
import { Post } from "@app/entities/post";
import { PostComment } from "@app/entities/post-comment";
import { PostService } from "@app/services/post.service";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-post-comment-modal",
  templateUrl: "./post-comment-form.modal.html",
  styleUrls: ["./post-comment-form.modal.scss"],
})
export class PostCommentFormModal implements OnInit, AfterViewInit {

  @Input() post: Post;
  @Input() postComment: PostComment = new PostComment();
  @ViewChild("postCommentBody", {static: true}) postCommentBody: any;

  constructor(private modalController: ModalController,
              private postService: PostService) {
    // Do nothing.
  }

  ngOnInit() {
    if (!this.post && this.postComment.post) {
      this.post = this.postComment.post;
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.postCommentBody.setFocus(true);
    }, 400);
  }

  async onClose() {
    await this.modalController.dismiss();
  }

  onCreate() {
    this.postService.createPostComment(this.post, this.postComment).subscribe((postComment) => {
      this.modalController.dismiss({
        postComment: postComment
      });
    });
  }

  onUpdate() {
    this.postService.updatePostComment(this.post, this.postComment).subscribe((postComment) => {
      this.modalController.dismiss({
        postComment: postComment
      });
    });
  }

  isPostable() {
    return this.postComment.body && this.postComment.body.length > 0;
  }

}
