import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppCommonModule } from "@app/app-common.module";
import { PostCommentFormModal } from "@app/modals/post-comment-form/post-comment-form.modal";

@NgModule({
  imports: [
    AppCommonModule,
    FormsModule
  ],
  declarations: [
    PostCommentFormModal
  ],
  entryComponents: [
    PostCommentFormModal
  ]
})
export class PostCommentFormModalModule {
}
