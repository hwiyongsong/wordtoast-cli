import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Chrome } from "@app/core/chrome";
import { Tracking } from "@app/core/tracking";
import { ImageService } from "@app/services/image.service";
import { ModalController } from "@ionic/angular";
import Cropper from "cropperjs";

@Component({
  selector: "app-image-edit-modal",
  templateUrl: "./image-edit.modal.html",
  styleUrls: ["./image-edit.modal.scss"],
})
export class ImageEditModal implements OnInit {

  @Input() sourceUrl: any;
  public cropper: any;
  public cropperReady: boolean = false;
  @ViewChild("canvasImage", {static: false}) canvasImageRef: ElementRef;

  constructor(private chrome: Chrome,
              private tracking: Tracking,
              private firebaseClient: FirebaseClient,
              private modalController: ModalController,
              private imageService: ImageService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackPageImpression("app-image-edit-modal");
  }

  async onClose() {
    await this.modalController.dismiss({});
  }

  onImageReady() {
    this.initCropper();
  }

  onCropperRotate() {
    this.cropper.rotate(90);
  }

  onUpload() {
    this.chrome.presentLoading().then(() => {
      const dataUrl = this.cropper.getCroppedCanvas().toDataURL("image/jpeg", 0.8);

      this.imageService.resize(dataUrl, 960, 960).subscribe((resizedDataUrl) => {
        this.imageService.createImageByDataUrl(resizedDataUrl).subscribe((image) => {
          this.chrome.dismissLoading();
          this.modalController.dismiss({
            image: image
          });
        });
      });
    });
  }

  private initCropper() {
    const previewCropperWidth = window.innerWidth;
    const previewCropperHeight = window.innerWidth;

    this.cropper = new Cropper(this.canvasImageRef.nativeElement, {
      viewMode: 3,
      dragMode: "move" as any,
      aspectRatio: 1 / 1,
      autoCropArea: 1,
      guides: false,
      center: false,
      cropBoxMovable: false,
      cropBoxResizable: false,
      minCanvasWidth: previewCropperWidth,
      minCanvasHeight: previewCropperHeight,
      minContainerWidth: previewCropperWidth,
      minContainerHeight: previewCropperHeight,
      minCropBoxWidth: previewCropperWidth,
      minCropBoxHeight: previewCropperHeight
    });

    this.cropperReady = true;
  }

}
