import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageEditModal } from "@app/modals/image-edit/image-edit.modal";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    ImageEditModal
  ],
  entryComponents: [
    ImageEditModal
  ]
})
export class ImageEditModalModule {
}
