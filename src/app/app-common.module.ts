import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FileSelectDirective } from "@app/directives/file-select.directive";
import { TextareaAutosizeDirective } from "@app/directives/textarea-autosize.directive";
import { SafePipe } from "@app/pipes/safehtml.pipe";
import { SafeurlPipe } from "@app/pipes/safeurl.pipe";
import { ShortNumberPipe } from "@app/pipes/short-number.pipe";
import { TimeAgoPipe } from "@app/pipes/time-ago.pipe";
import { IonicModule } from "@ionic/angular";
import { ClipboardModule } from "ngx-clipboard";

@NgModule({
  declarations: [
    FileSelectDirective,
    TextareaAutosizeDirective,
    SafeurlPipe,
    SafePipe,
    ShortNumberPipe,
    TimeAgoPipe
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    ReactiveFormsModule,
    ClipboardModule
  ],
  exports: [
    CommonModule,
    IonicModule,
    RouterModule,
    ReactiveFormsModule,
    ClipboardModule,
    TextareaAutosizeDirective,
    SafeurlPipe,
    SafePipe,
    ShortNumberPipe,
    TimeAgoPipe
  ]
})
export class AppCommonModule {
}
