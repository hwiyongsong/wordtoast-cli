import { Component } from "@angular/core";
import { AppContext } from "@app/app.context";
import { Session } from "@app/core/session";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {

  constructor(private context: AppContext,
              private session: Session,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar) {
    this.initializeApp();
  }

  initializeApp() {
    this.context.onReady().then(() => {
      if (this.context.isCordova()) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }

      this.session.refreshAll();
    });
  }
}
