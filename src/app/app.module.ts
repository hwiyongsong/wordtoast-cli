import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireAuthGuard } from "@angular/fire/auth-guard";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { ServiceWorkerModule } from "@angular/service-worker";
import { MenuBlockModule } from "@app/blocks/menu/menu.module";
import { AuthTokenInterceptor } from "@app/core/auth-token.interceptor";
import { TransitionUtils } from "@app/utils/transition-utils";
import { environment } from "@environments/environment";
// import { Facebook } from "@ionic-native/facebook/ngx";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { LoggerModule } from "ngx-logger";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  imports: [
    AppRoutingModule,
    BrowserModule,
    IonicModule.forRoot({
      mode: "md",
      navAnimation: TransitionUtils.noAnimation
    }),
    LoggerModule.forRoot(environment.logging),
    ServiceWorkerModule.register("ngsw-worker.js", {enabled: environment.production}),
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase.config),
    AngularFireAuthModule,
    AngularFireStorageModule,
    MenuBlockModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // Facebook,
    AngularFireAuthGuard,
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule {
}
