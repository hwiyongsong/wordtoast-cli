import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { FriendRequest } from "@app/entities/friend-request";
import { UserService } from "@app/services/user.service";

@Injectable({
  providedIn: "root"
})
export class FriendRequestResolver implements Resolve<FriendRequest> {

  constructor(private userService: UserService) {
    // Do nothing.
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const friendRequestId = route.params.friendRequestId;
    return this.userService.getFriendRequestById(friendRequestId);
  }
}
