import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";

@Injectable({
  providedIn: "root"
})
export class UserResolver implements Resolve<User> {

  constructor(private userService: UserService) {
    // Do nothing.
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const userId = route.params.userId;
    return this.userService.getUser(userId);
  }
}
