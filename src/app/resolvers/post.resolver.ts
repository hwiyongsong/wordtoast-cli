import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Post } from "@app/entities/post";
import { PostService } from "@app/services/post.service";

@Injectable({
  providedIn: "root"
})
export class PostResolver implements Resolve<Post> {

  constructor(private postService: PostService) {
    // Do nothing.
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const postId = route.params.postId;
    return this.postService.getPost(postId);
  }
}
