import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Topic } from "@app/entities/topic";
import { TopicService } from "@app/services/topic.service";

@Injectable({
  providedIn: "root"
})
export class TopicResolver implements Resolve<Topic> {

  constructor(private topicService: TopicService) {
    // Do nothing.
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const topicId = route.params.topicId;
    return this.topicService.getTopic(topicId);
  }
}
