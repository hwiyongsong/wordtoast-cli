import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Hashtag } from "@app/entities/hashtag";
import { HashtagService } from "@app/services/hashtag.service";

@Injectable({
  providedIn: "root"
})
export class HashtagResolver implements Resolve<Hashtag> {

  constructor(private hashtagService: HashtagService) {
    // Do nothing.
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const hashtagId = route.params.hashtagId;
    return this.hashtagService.getHashtag(hashtagId);
  }
}
