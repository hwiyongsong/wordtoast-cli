import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { BlankPopover } from "@app/popovers/blank/blank.popover";
import { PostFormPopover } from "@app/popovers/post-form/post-form.popover";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    PostFormPopover
  ],
  exports: [
    PostFormPopover
  ]
})
export class PostFormPopoverModule {
}
