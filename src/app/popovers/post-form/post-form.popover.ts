import { Component, Input, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { QueryResult } from "../../entities/query-result";
import { Post } from "@app/entities/post";
import { TopicService } from "@app/services/topic.service";
import { PopoverController } from "@ionic/angular";

@Component({
  selector: "app-post-form-popover",
  templateUrl: "./post-form.popover.html",
  styleUrls: ["./post-form.popover.scss"],
})
export class PostFormPopover implements OnInit {

  @Input() post: Post;

  public selectedTopicId: string;
  public topicsFeed: QueryResult = new QueryResult();

  constructor(private navigation: Navigation,
              private popoverController: PopoverController,
              private topicService: TopicService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.selectedTopicId = this.post.topic ? this.post.topic.entityId : null;
    this.topicService.queryTopics().subscribe((topicsFeed) => {
      this.topicsFeed.refresh(topicsFeed);
    });
  }

  onTopic(event: any) {
    this.selectedTopicId = event.target.value;
    this.post.topic = this.topicsFeed.find(this.selectedTopicId);
    this.popoverController.dismiss();
  }

}
