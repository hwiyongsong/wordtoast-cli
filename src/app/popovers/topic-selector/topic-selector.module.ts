import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { TopicSelectorPopover } from "@app/popovers/topic-selector/topic-selector.popover";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    TopicSelectorPopover
  ],
  exports: [
    TopicSelectorPopover
  ]
})
export class TopicSelectorPopoverModule {
}
