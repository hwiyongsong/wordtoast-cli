import { Component, Input } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Post } from "@app/entities/post";
import { Topic } from "@app/entities/topic";
import { PopoverController } from "@ionic/angular";

@Component({
  selector: "app-topic-selector-popover",
  templateUrl: "./topic-selector.popover.html",
  styleUrls: ["./topic-selector.popover.scss"],
})
export class TopicSelectorPopover {

  @Input() post: Post = new Post();

  constructor(public chrome: Chrome,
              private popoverController: PopoverController) {
    // Do nothing.
  }

  onSelectTopic(topic: Topic) {
    this.post.topic = topic;
    this.popoverController.dismiss();
  }

}
