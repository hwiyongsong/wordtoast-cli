import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { VisibilitySelectorPopover } from "@app/popovers/visibility-selector/visibility-selector.popover";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    VisibilitySelectorPopover
  ],
  exports: [
    VisibilitySelectorPopover
  ]
})
export class VisibilitySelectorPopoverModule {
}
