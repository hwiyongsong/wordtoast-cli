import { Component, Input } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Post } from "@app/entities/post";
import { Visibility } from "@app/entities/visibility";
import { PopoverController } from "@ionic/angular";

@Component({
  selector: "app-visibility-selector-popover",
  templateUrl: "./visibility-selector.popover.html",
  styleUrls: ["./visibility-selector.popover.scss"],
})
export class VisibilitySelectorPopover {

  @Input() post: Post = new Post();
  public visibilityEnum = Visibility;

  constructor(public chrome: Chrome,
              private popoverController: PopoverController) {
    // Do nothing.
  }

  onSelectVisibility(visibility: Visibility) {
    this.post.visibility = visibility;
    this.popoverController.dismiss();
  }

}
