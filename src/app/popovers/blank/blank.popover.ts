import { Component } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { PopoverController } from "@ionic/angular";

@Component({
  selector: "app-blank-popover",
  templateUrl: "./blank.popover.html",
  styleUrls: ["./blank.popover.scss"],
})
export class BlankPopover {

  constructor(private navigation: Navigation,
              private popoverController: PopoverController) {
    // Do nothing.
  }

}
