import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { BlankPopover } from "@app/popovers/blank/blank.popover";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    BlankPopover
  ],
  exports: [
    BlankPopover
  ]
})
export class BlankPopoverModule {
}
