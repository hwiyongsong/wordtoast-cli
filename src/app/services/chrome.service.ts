import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { ChromeHeader } from "@app/entities/chrome-header";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ChromeService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  getChromeHeader(): Observable<ChromeHeader> {
    const endpoint = "/v1/chrome/header";
    return this.apiClient.get(endpoint).pipe(map(response => new ChromeHeader(response.data)));
  }

}
