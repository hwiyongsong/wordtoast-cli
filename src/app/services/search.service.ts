import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { RequestParameter } from "@app/core/request-parameter";
import { SearchResult } from "@app/entities/search-result";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class SearchService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  search(input: string): Observable<SearchResult> {
    const endpoint = "/v1/search";

    const params: any = {};
    params[RequestParameter.INPUT] = input;

    return this.apiClient.get(endpoint, params).pipe(map(response => new SearchResult(response.data)));
  }

}
