import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { QueryResult } from "../entities/query-result";
import { Query } from "@app/entities/query";
import { User } from "@app/entities/user";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class MyService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  public queryMyself(): Observable<User> {
    const endpoint = "/v1/me";
    return this.apiClient.get(endpoint).pipe(map(response => new User(response.data)));
  }

  public queryMyPosts(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/me/posts";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryMyBookmarks(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/me/bookmarks";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryMyFollowers(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/me/followers";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryMyFollowings(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/me/followings";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryMyNotifications(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/me/notifications";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public countMyNotifications(): Observable<number> {
    const endpoint = "/v1/me/notifications/count";
    return this.apiClient.get(endpoint).pipe(map(response => response.data));
  }

  public markMyNotificationsAsRead(): Observable<number> {
    const endpoint = "/v1/me/notifications/mark-as-read";
    return this.apiClient.post(endpoint).pipe(map(response => response.data));
  }

}
