import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { QueryResult } from "../entities/query-result";
import { Query } from "@app/entities/query";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class HomeService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  queryHomeFeed(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/home/feed";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

}
