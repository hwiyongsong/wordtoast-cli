import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { Hashtag } from "@app/entities/hashtag";
import { Query } from "@app/entities/query";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { QueryResult } from "../entities/query-result";

@Injectable({
  providedIn: "root"
})
export class HashtagService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  public queryHashtags(query: Query = new Query()): Observable<QueryResult> {
    const endpoint = "/v1/hashtags";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public getHashtag(hashtagId: string): Observable<Hashtag> {
    const endpoint = "/v1/hashtags/" + hashtagId;
    return this.apiClient.get(endpoint).pipe(map(response => new Hashtag(response.data)));
  }

}
