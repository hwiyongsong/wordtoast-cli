import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { QueryResult } from "../entities/query-result";
import { FriendRequest } from "@app/entities/friend-request";
import { FriendRequestStatus } from "@app/entities/friend-request-status";
import { Query } from "@app/entities/query";
import { User } from "@app/entities/user";
import { UserBlock } from "@app/entities/user-block";
import { UserFollow } from "@app/entities/user-follow";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class UserService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  public queryUsers(query: Query): Observable<QueryResult> {
    const endpoint = "/v1/users";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public createUser(user: User): Observable<User> {
    const endpoint = "/v1/users";
    return this.apiClient.post(endpoint, user.toData()).pipe(map(response => new User(response.data)));
  }

  public getUser(userId: string): Observable<User> {
    const endpoint = "/v1/users/" + userId;
    return this.apiClient.get(endpoint).pipe(map(response => new User(response.data)));
  }

  public updateUser(user: User): Observable<User> {
    const endpoint = "/v1/users/" + user.entityId;
    return this.apiClient.put(endpoint, user.toData()).pipe(map(response => new User(response.data)));
  }

  public queryUserPosts(userId: string, query: Query): Observable<QueryResult> {
    const endpoint = "/v1/users/" + userId + "/posts";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryUserBookmarks(userId: string, query: Query): Observable<QueryResult> {
    const endpoint = "/v1/users/" + userId + "/bookmarks";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryUserFollowings(userId: string, query: Query): Observable<QueryResult> {
    const endpoint = "/v1/users/" + userId + "/followings";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public queryUsersFollowers(userId: string, query: Query): Observable<QueryResult> {
    const endpoint = "/v1/users/" + userId + "/followers";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public createUserFollow(user: User): Observable<UserFollow> {
    user.following = true;
    user.followerCount++;

    const endpoint = "/v1/users/" + user.entityId + "/follows";
    return this.apiClient.post(endpoint).pipe(map(response => new UserFollow(response.data)));
  }

  public removeUserFollow(user: User): Observable<UserFollow> {
    user.following = false;

    if (user.followerCount > 0) {
      user.followerCount--;
    }

    const endpoint = "/v1/users/" + user.entityId + "/follows";
    return this.apiClient.delete(endpoint);
  }

  public queryUserFriends(userId: string, query: Query): Observable<QueryResult> {
    const endpoint = "/v1/users/" + userId + "/friends";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public createFriendRequest(friendRequest: FriendRequest): Observable<FriendRequest> {
    friendRequest.user.friending = true;

    const endpoint = "/v1/friend-requests";
    return this.apiClient.post(endpoint, friendRequest.toData()).pipe(map(response => new FriendRequest(response.data)));
  }

  public getFriendRequestById(friendRequestId: string): Observable<FriendRequest> {
    const endpoint = "/v1/friend-requests/" + friendRequestId;
    return this.apiClient.get(endpoint).pipe(map(response => new FriendRequest(response.data)));
  }

  public acceptFriendRequest(friendRequestId: string): Observable<FriendRequest> {
    const endpoint = "/v1/friend-requests/" + friendRequestId;
    const data = {
      status: FriendRequestStatus[FriendRequestStatus.ACCEPTED]
    };
    return this.apiClient.put(endpoint, data).pipe(map(response => new FriendRequest(response.data)));
  }

  public declineFriendRequest(friendRequestId: string): Observable<FriendRequest> {
    const endpoint = "/v1/friend-requests/" + friendRequestId;
    const data = {
      status: FriendRequestStatus[FriendRequestStatus.DECLINED]
    };
    return this.apiClient.put(endpoint, data).pipe(map(response => new FriendRequest(response.data)));
  }

  /**** USER BLOCKS ****/

  public createUserBlock(user: User): Observable<UserBlock> {
    user.blocked = true;

    const endpoint = "/v1/users/" + user.entityId + "/blocks";
    return this.apiClient.post(endpoint).pipe(map(response => new UserBlock(response.data)));
  }

  public removeUserBlock(user: User): Observable<void> {
    user.blocked = false;

    const endpoint = "/v1/users/" + user.entityId + "/blocks";
    return this.apiClient.delete(endpoint);
  }

}
