import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { QueryResult } from "../entities/query-result";
import { Query } from "@app/entities/query";
import { Topic } from "@app/entities/topic";
import { TopicFollow } from "@app/entities/topic-follow";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class TopicService {

  constructor(private apiClient: ApiClient) {
    // Do nothing.
  }

  public queryTopics(query: Query = new Query()): Observable<QueryResult> {
    const endpoint = "/v1/topics";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public getTopic(topicId: string): Observable<Topic> {
    const endpoint = "/v1/topics/" + topicId;
    return this.apiClient.get(endpoint).pipe(map(response => new Topic(response.data)));
  }

  public createTopicFollow(topic: Topic): Observable<TopicFollow> {
    const endpoint = "/v1/topics/" + topic.entityId + "/follows";
    const sequence = this.apiClient.post(endpoint).pipe(map(response => new TopicFollow(response.data)));

    topic.followerCount++;
    topic.following = true;

    return sequence;
  }

  public removeTopicFollow(topic: Topic): Observable<TopicFollow> {
    const endpoint = "/v1/topics/" + topic.entityId + "/follows";
    const sequence = this.apiClient.delete(endpoint);

    topic.following = false;

    if (topic.followerCount > 0) {
      topic.followerCount--;
    }

    return sequence;
  }

}
