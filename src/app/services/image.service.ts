import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { FirebaseClient } from "@app/clients/firebase.client";
import { Image } from "@app/entities/image";
import { EntityUtils } from "@app/utils/entity-utils";
import { ImageUtils } from "@app/utils/image-utils";
import { Observable } from "rxjs";
import { flatMap, map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ImageService {

  constructor(private apiClient: ApiClient,
              private firebaseClient: FirebaseClient) {
    // Do nothing.
  }

  createImageBySourceUrl(sourceUrl: string): Observable<Image> {
    const bucket = FirebaseClient.BUCKET_CONTENTS;
    const filename = EntityUtils.generateFriendlyId() + ".jpg";
    const mimeType = "image/jpeg";
    const endpoint = "/v1/images";
    const data: any = {
      sourceUrl: sourceUrl,
      bucket: bucket,
      filename: filename
    };
    return this.apiClient.post(endpoint, data).pipe(map(response => new Image(response.data)));
  }

  createImageByDataUrl(dataUrl: string): Observable<Image> {
    const bucket = FirebaseClient.BUCKET_CONTENTS;
    const filename = EntityUtils.generateFriendlyId() + ".jpg";
    const mimeType = "image/jpeg";

    return this.firebaseClient.uploadDataUrlToStorage(bucket, filename, mimeType, dataUrl).pipe(
      flatMap((storageUrl) => {
        const endpoint = "/v1/images";
        const data: any = {
          sourceUrl: storageUrl,
          bucket: bucket,
          filename: filename
        };
        return this.apiClient.post(endpoint, data).pipe(map(response => new Image(response.data)));
      })
    );
  }

  getImage(imageId: string): Observable<Image> {
    const endpoint = "/v1/images/" + imageId;
    return this.apiClient.get(endpoint).pipe(map(response => new Image(response.data)));
  }

  readAsDataUrl(blob: Blob): Observable<string | ArrayBuffer> {
    return new Observable((observer) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);

      reader.onload = () => {
        observer.next(reader.result);
        observer.complete();
      };

      reader.onerror = (error) => {
        observer.error(error);
      };
    });
  }

  resize(dataUrl, maxWidth, maxHeight): Observable<string> {
    return new Observable((observer) => {
      ImageUtils.resize(dataUrl, maxWidth, maxHeight, (resizedDataUrl) => {
        observer.next(resizedDataUrl);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

}
