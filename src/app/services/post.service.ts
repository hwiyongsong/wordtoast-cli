import { Injectable } from "@angular/core";
import { ApiClient } from "@app/clients/api.client";
import { Event } from "@app/core/event";
import { EventBus } from "@app/core/event-bus";
import { Post } from "@app/entities/post";
import { PostBlock } from "@app/entities/post-block";
import { PostBookmark } from "@app/entities/post-bookmark";
import { PostComment } from "@app/entities/post-comment";
import { PostReaction } from "@app/entities/post-reaction";
import { Query } from "@app/entities/query";
import { ReactionType } from "@app/entities/reaction-type";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { QueryResult } from "../entities/query-result";

@Injectable({
  providedIn: "root"
})
export class PostService {

  constructor(private apiClient: ApiClient,
              private eventBus: EventBus) {
    // Do nothing.
  }

  /**** POSTS ****/

  public queryPosts(query?: Query): Observable<QueryResult> {
    const endpoint = "/v1/posts";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public createPost(post: Post): Observable<Post> {
    const endpoint = "/v1/posts";
    const sequence = this.apiClient.post(endpoint, post.toData()).pipe(map(response => new Post(response.data)));

    sequence.subscribe((persistedPost) => {
      this.eventBus.emit(Event.POST_CREATED, persistedPost);
    });

    return sequence;
  }

  public getPost(postId: string): Observable<Post> {
    const endpoint = "/v1/posts/" + postId;
    return this.apiClient.get(endpoint).pipe(map(response => new Post(response.data)));
  }

  public updatePost(post: Post): Observable<Post> {
    const endpoint = "/v1/posts/" + post.entityId;
    const sequence = this.apiClient.put(endpoint, post.toData()).pipe(map(response => new Post(response.data)));

    sequence.subscribe((persistedPost) => {
      this.eventBus.emit(Event.POST_UPDATED, persistedPost);
    });

    return sequence;
  }

  public removePost(post: Post): Observable<Post> {
    const endpoint = "/v1/posts/" + post.entityId;
    const sequence = this.apiClient.delete(endpoint);

    sequence.subscribe(() => {
      this.eventBus.emit(Event.POST_REMOVED, post);
    });

    return sequence;
  }

  /**** POST REACTIONS ****/

  public queryPostReactions(post: Post, query?: Query): Observable<QueryResult> {
    const endpoint = "/v1/posts/" + post.entityId + "/reactions";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public createPostReaction(post: Post, reactionType: ReactionType = ReactionType.LIKE): Observable<PostReaction> {
    post.reacted = true;
    post.reactionCount++;

    const endpoint = "/v1/posts/" + post.entityId + "/reactions";
    const data = {
      reactionType: ReactionType[reactionType]
    };
    return this.apiClient.post(endpoint, data).pipe(map(response => new PostReaction(response.data)));
  }

  public removePostReaction(post: Post): Observable<void> {
    post.reacted = false;
    post.reactionCount--;

    const endpoint = "/v1/posts/" + post.entityId + "/reactions";
    return this.apiClient.delete(endpoint).pipe(map(response => null));
  }

  /**** POST COMMENTS ****/

  public queryPostComments(post: Post, query: Query): Observable<QueryResult> {
    console.log("** queryPostComments");
    const endpoint = "/v1/posts/" + post.entityId + "/comments";
    return this.apiClient.get(endpoint, query.toData()).pipe(map(response => new QueryResult(response.data)));
  }

  public createPostComment(post: Post, postComment: PostComment): Observable<PostComment> {
    post.commentCount++;

    const endpoint = "/v1/posts/" + post.entityId + "/comments";
    return this.apiClient.post(endpoint, postComment.toData()).pipe(map(response => new PostComment(response.data)));
  }

  public updatePostComment(post: Post, postComment: PostComment): Observable<PostComment> {
    const endpoint = "/v1/posts/" + post.entityId + "/comments/" + postComment.entityId;
    const sequence = this.apiClient.put(endpoint, postComment.toData()).pipe(map(response => new PostComment(response.data)));

    sequence.subscribe((persistedPostComment) => {
      postComment.body = persistedPostComment.body;
      postComment.updatedAt = persistedPostComment.updatedAt;
    });

    return sequence;
  }

  public removePostComment(post: Post, postComment: PostComment): Observable<void> {
    post.commentCount--;
    postComment.invalidated = true;

    const endpoint = "/v1/posts/" + post.entityId + "/comments/" + postComment.entityId;
    return this.apiClient.delete(endpoint).pipe(map(response => null));
  }

  /**** POST BOOKMARKS ****/

  public createPostBookmark(post: Post): Observable<PostBookmark> {
    post.bookmarked = true;
    post.bookmarkCount++;

    const endpoint = "/v1/posts/" + post.entityId + "/bookmarks";
    return this.apiClient.post(endpoint).pipe(map(response => new PostBookmark(response.data)));
  }

  public removePostBookmark(post: Post): Observable<void> {
    post.bookmarked = false;
    post.bookmarkCount--;

    const endpoint = "/v1/posts/" + post.entityId + "/bookmarks";
    return this.apiClient.delete(endpoint).pipe(map(response => null));
  }

  /**** POST BLOCKS ****/

  public createPostBlock(post: Post): Observable<PostBlock> {
    post.blocked = true;

    const endpoint = "/v1/posts/" + post.entityId + "/blocks";
    return this.apiClient.post(endpoint).pipe(map(response => new PostBlock(response.data)));
  }

  public removePostBlock(post: Post): Observable<void> {
    post.blocked = false;

    const endpoint = "/v1/posts/" + post.entityId + "/blocks";
    return this.apiClient.delete(endpoint);
  }

}
