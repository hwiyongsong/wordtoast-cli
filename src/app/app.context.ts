import { Injectable } from "@angular/core";
import { environment } from "@environments/environment";
import { Platform } from "@ionic/angular";
import { NGXLogger } from "ngx-logger";

@Injectable({
  providedIn: "root"
})
export class AppContext {

  private apiBaseUrl: string;

  constructor(private logger: NGXLogger,
              private platform: Platform) {
    logger.info("** Detected environment: " + environment.name);
    logger.info("** Detected platforms: " + platform.platforms());
  }

  onReady(): Promise<string> {
    return this.platform.ready();
  }

  getRouteHost(): string {
    return window.location.origin;
  }

  getRoutePath(): string {
    return window.location.pathname;
  }

  getApiBaseUrl(): string {
    if (!this.apiBaseUrl) {
      this.apiBaseUrl = environment.apiBaseUrl.replace("localhost", window.location.hostname);
    }

    return this.apiBaseUrl;
  }

  getEnvironmentName(): string {
    return environment.name;
  }

  isCordova(): boolean {
    return this.platform.is("cordova");
  }

  isMobile(): boolean {
    return this.platform.is("mobile");
  }

}
