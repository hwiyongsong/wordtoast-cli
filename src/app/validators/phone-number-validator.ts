import { FormControl } from "@angular/forms";

export class PhoneNumberValidator {

  static isValid(control: FormControl) {
    const valid = /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$/.test(control.value);

    if (valid) {
      return null;
    }

    return { "invalidPhoneNumber": true };
  }

}
