import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFireStorage } from "@angular/fire/storage";
import { AppContext } from "@app/app.context";
// import { Facebook } from "@ionic-native/facebook/ngx";
import * as firebase from "firebase";
import { NGXLogger } from "ngx-logger";
import { Observable, of } from "rxjs";
import { from } from "rxjs/internal/observable/from";
import { take } from "rxjs/operators";
import UserCredential = firebase.auth.UserCredential;

@Injectable({
  providedIn: "root"
})
export class FirebaseClient {

  public static readonly BUCKET_CONTENTS = "contents";

  constructor(private context: AppContext,
              // private facebook: Facebook,
              private firebaseAuth: AngularFireAuth,
              private firebaseStorage: AngularFireStorage,
              private logger: NGXLogger) {
    // Do nothing.
  }

  /* AUTHENTICATION */

  getAuthState(): Observable<firebase.User | null> {
    return this.firebaseAuth.authState.pipe(take(1));
  }

  createUserWithEmailAndPassword(email: string, password: string): Observable<UserCredential> {
    const promise = firebase.auth().createUserWithEmailAndPassword(email, password);
    return from(promise);
  }

  signInWithEmailAndPassword(email: string, password: string): Observable<UserCredential> {
    const promise = firebase.auth().signInWithEmailAndPassword(email, password);
    return from(promise);
  }

  signInWithFacebook(): Observable<UserCredential> {
    if (this.context.isCordova()) {
      this.logger.debug("** Signing with Facebook Native");
      return this.signInWithFacebookNative();
    } else {
      this.logger.debug("** Signing with Facebook Web");
      return this.signInWithFacebookWeb();
    }
  }

  signOut(): Observable<void> {
    const promise = this.firebaseAuth.signOut();
    return from(promise);
  }

  sendPasswordResetEmail(email: string): Observable<void> {
    const promise = firebase.auth().sendPasswordResetEmail(email);
    return from(promise);
  }

  updateUserDisplayName(displayName: string) {
    this.getAuthState().subscribe((firebaseUser) => {
      firebaseUser.updateProfile({
        displayName: displayName
      });
    });
  }

  updateUserProfileImageUrl(profileImageUrl: string) {
    this.getAuthState().subscribe((firebaseUser) => {
      firebaseUser.updateProfile({
        photoURL: profileImageUrl
      });
    });
  }

  private signInWithFacebookNative(): Observable<UserCredential> {
    /*
    const promise = this.facebook.login(["public_profile", "email"]).then((response: FacebookLoginResponse) => {
      const facebookAccessToken = response.authResponse.accessToken;
      const facebookCredential = firebase.auth.FacebookAuthProvider.credential(facebookAccessToken);
      return this.firebaseAuth.auth.signInWithCredential(facebookCredential);
    });

    return from(promise);
     */

    return of(null);
  }

  private signInWithFacebookWeb(): Observable<UserCredential> {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope("public_profile");
    provider.addScope("email");

    const promise = firebase.auth().signInWithPopup(provider);
    return from(promise);
  }

  /* STORAGE */

  uploadDataUrlToStorage(bucket: string, filename: string, mimeType: string, dataUrl: string): Observable<string> {
    return new Observable((observer) => {
      const fileReference = this.firebaseStorage.storage.ref(bucket).child(filename);
      const sanitizedDataUrl = this.sanitizeDataUrl(dataUrl);
      const metadata = {
        contentType: mimeType
      };

      fileReference.putString(sanitizedDataUrl, "base64", metadata).then(() => {
        fileReference.getDownloadURL().then((storageUrl) => {
          observer.next(storageUrl);
          observer.complete();
        });
      }, (error) => {
        observer.error(error);
      });
    });
  }

  private sanitizeDataUrl(dataUrl): string {
    const BASE64_PREFIX = ";base64,";
    const tokens = dataUrl.split(BASE64_PREFIX);
    return tokens.length > 1 ? tokens[1] : tokens[0];
  }

}
