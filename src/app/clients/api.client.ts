import { HttpClient, HttpParameterCodec, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppContext } from "@app/app.context";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ApiClient {

  constructor(private appContext: AppContext,
              private httpClient: HttpClient) {
    // Do nothing.
  }

  get(endpoint: string, params: any = {}): Observable<any> {
    const url = this.buildUrl(endpoint);
    const body = {
      "params": params
    };
    return this.httpClient.get(url, body).pipe(share());
  }

  post(endpoint: string, body: any = {}): Observable<any> {
    const url = this.buildUrl(endpoint);
    const data = this.serializeAsHttpParams(body);
    return this.httpClient.post(url, data).pipe(share());
  }

  put(endpoint: string, body: any = {}): Observable<any> {
    const url = this.buildUrl(endpoint);
    const data = this.serializeAsHttpParams(body);
    return this.httpClient.put(url, data).pipe(share());
  }

  delete(endpoint: string): Observable<any> {
    const url = this.buildUrl(endpoint);
    return this.httpClient.delete(url).pipe(share());
  }

  private buildUrl(endpoint: string): string {
    return this.appContext.getApiBaseUrl() + endpoint;
  }

  private serializeAsHttpParams(body: any): HttpParams {
    let params = new HttpParams({
      encoder: new HttpUrlEncodingCodec()
    });

    for (const paramKey of Object.keys(body)) {
      const paramValue = body[paramKey] || "";
      params = params.set(paramKey, paramValue);
    }

    return params;
  }

}

export class HttpUrlEncodingCodec implements HttpParameterCodec {
  encodeKey(key: string): string {
    return encodeURIComponent(key);
  }

  encodeValue(value: string): string {
    return encodeURIComponent(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string) {
    return decodeURIComponent(value);
  }
}
