import { AbstractEntity } from "@app/entities/abstract.entity";
import { Entity } from "@app/entities/entity";
import { QueryResult } from "./query-result";
import { EntityUtils } from "@app/utils/entity-utils";

export class Feed extends AbstractEntity {

  public entities: Array<any> = [];
  public limit: number = 10;
  public offset: number = 0;
  public ready: boolean = false;
  public eof: boolean = false;

  [Symbol.iterator]() {
    const entities = this.entities;
    let step = 0;

    return {
      next() {
        if (step < entities.length) {
          return {
            done: false,
            value: entities[step++]
          };
        } else {
          return {
            done: true,
            value: undefined
          };
        }
      }
    };
  }

  find(entityId: string): any {
    return this.entities.find((entity) => entity.entityId === entityId);
  }

  add(entity: any) {
    this.entities.push(entity);
    this.ready = true;
  }

  addAll(entities: Array<any>) {
    this.entities = this.entities.concat(entities);
    this.ready = true;
  }

  addToFirst(entity: any) {
    this.entities.unshift(entity);
    this.ready = true;
  }

  update(entity: any) {
    const index = EntityUtils.getIndexOf(this.entities, entity);

    if (index >= 0) {
      this.entities[index] = entity;
    } else {
      this.addToFirst(entity);
    }
  }

  remove(entity: any) {
    const index = EntityUtils.getIndexOf(this.entities, entity);

    if (index >= 0) {
      this.entities.splice(index, 1);
    }
  }

  append(queryResult: QueryResult) {
    this.addAll(queryResult.entities);

    if (queryResult.size() < this.limit) {
      this.eof = true;
    }
  }

  refresh(queryResult: QueryResult) {
    this.clear();
    this.append(queryResult);
  }

  size() {
    return this.ready ? this.entities.length : -1;
  }

  clear() {
    this.entities = [];
    this.offset = 0;
    this.ready = false;
    this.eof = false;
  }

  getNextOffset() {
    return this.offset + this.entities.length;
  }

}
