import { AbstractEntity } from "@app/entities/abstract.entity";

export class NavItem extends AbstractEntity {

  public title: string;
  public url: string;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.title) {
        this.title = data.title;
      }

      if (data.url) {
        this.url = data.url;
      }
    }
  }

}
