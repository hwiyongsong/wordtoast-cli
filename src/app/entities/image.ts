import { AbstractEntity } from "@app/entities/abstract.entity";

export class Image extends AbstractEntity {

  public sourceUrl: string;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.sourceUrl) {
        this.sourceUrl = data.sourceUrl;
      }
    }
  }

  toData() {
    const data: any = {};

    if (this.sourceUrl) {
      data.sourceUrl = this.sourceUrl;
    }

    return data;
  }

}
