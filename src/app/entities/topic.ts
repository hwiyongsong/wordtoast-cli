import { AbstractEntity } from "@app/entities/abstract.entity";
import { Image } from "@app/entities/image";

export class Topic extends AbstractEntity {

  public title: string;
  public description: string;
  public posterImage: Image;
  public followerCount: number = 0;
  public following: boolean;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.title) {
        this.title = data.title;
      }

      if (data.description) {
        this.description = data.description;
      }

      if (data.posterImage) {
        this.posterImage = new Image(data.posterImage);
      }

      if (data.followerCount) {
        this.followerCount = data.followerCount;
      }

      if (data.following) {
        this.following = data.following;
      }
    }
  }

}
