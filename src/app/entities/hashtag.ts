import { AbstractEntity } from "@app/entities/abstract.entity";

export class Hashtag extends AbstractEntity {

  public name: string;
  public postCount: number = 0;
  public followerCount: number = 0;
  public following: boolean;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.name) {
        this.name = data.name;
      }

      if (data.postCount) {
        this.postCount = data.postCount;
      }

      if (data.followerCount) {
        this.followerCount = data.followerCount;
      }

      if (data.following) {
        this.following = data.following;
      }
    }
  }

}
