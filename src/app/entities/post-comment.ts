import { AbstractEntity } from "@app/entities/abstract.entity";
import { Post } from "@app/entities/post";
import { User } from "@app/entities/user";
import { DateUtils } from "@app/utils/date-utils";

export class PostComment extends AbstractEntity {

  public post: Post;
  public body: string;
  public createdBy: User;
  public createdAt: Date;
  public updatedAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.post) {
        this.post = new Post(data.post);
      }

      if (data.body) {
        this.body = data.body;
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }

      if (data.updatedAt) {
        this.updatedAt = DateUtils.parseDate(data.updatedAt);
      }
    }
  }

  isEditableBy(user: User) {
    return this.createdBy.entityId === user.entityId;
  }

  toData(): any {
    const data: any = {};

    if (this.body) {
      data.body = this.body;
    }

    return data;
  }

}
