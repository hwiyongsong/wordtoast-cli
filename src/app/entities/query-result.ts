import { AbstractEntity } from "@app/entities/abstract.entity";
import { Entity } from "@app/entities/entity";
import { EntityUtils } from "@app/utils/entity-utils";

export class QueryResult extends AbstractEntity {

  public entities: Array<any> = [];
  public limit: number = 10;
  public offset: number = 0;
  public ready: boolean = false;
  public eof: boolean = false;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.meta) {
        this.limit = data.meta.limit;
        this.offset = data.meta.offset;
      }

      if (data.entities) {
        this.entities = data.entities.map((entityData) => Entity.of(entityData));
      }

      if (this.size() < this.limit) {
        this.eof = true;
      }
    }
  }

  static of(entities: Array<any>): QueryResult {
    const feed = new QueryResult();
    feed.addAll(entities);
    return feed;
  }

  [Symbol.iterator]() {
    const entities = this.entities;
    let step = 0;

    return {
      next() {
        if (step < entities.length) {
          return {
            done: false,
            value: entities[step++]
          };
        } else {
          return {
            done: true,
            value: undefined
          };
        }
      }
    };
  }

  find(entityId: string): any {
    return this.entities.find((entity) => entity.entityId === entityId);
  }

  add(entity: any) {
    this.entities.push(entity);
    this.ready = true;
  }

  addAll(entities: Array<any>) {
    this.entities = this.entities.concat(entities);
    this.ready = true;
  }

  addToFirst(entity: any) {
    this.entities.unshift(entity);
    this.ready = true;
  }

  update(entity: any) {
    const index = EntityUtils.getIndexOf(this.entities, entity);

    if (index >= 0) {
      this.entities[index] = entity;
    } else {
      this.addToFirst(entity);
    }
  }

  remove(entity: any) {
    const index = EntityUtils.getIndexOf(this.entities, entity);

    if (index >= 0) {
      this.entities.splice(index, 1);
    }
  }

  append(feed: QueryResult) {
    this.addAll(feed.entities);

    if (feed.size() < this.limit) {
      this.eof = true;
    }
  }

  refresh(feed: QueryResult) {
    this.clear();
    this.append(feed);
  }

  size() {
    return this.ready ? this.entities.length : -1;
  }

  clear() {
    this.entities = [];
    this.offset = 0;
    this.ready = false;
    this.eof = false;
  }

  getNextOffset() {
    return this.offset + this.entities.length;
  }

}
