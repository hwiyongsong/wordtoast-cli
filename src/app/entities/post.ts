import { AbstractEntity } from "@app/entities/abstract.entity";
import { Hashtag } from "@app/entities/hashtag";
import { Image } from "@app/entities/image";
import { PostType } from "@app/entities/post-type";
import { PublishStatus } from "@app/entities/publish-status";
import { Topic } from "@app/entities/topic";
import { User } from "@app/entities/user";
import { Visibility } from "@app/entities/visibility";
import { DateUtils } from "@app/utils/date-utils";

export class Post extends AbstractEntity {

  public postType: PostType;
  public title: string;
  public subtitle: string;
  public body: string;
  public formattedBody: string;
  public coverImage: Image;
  public topic: Topic;
  public prompt: Post;
  public hashtagText: string;
  public slug: string;
  public visibility: Visibility = Visibility.PUBLIC;
  public publishStatus: PublishStatus = PublishStatus.DRAFT;
  public reactionCount: number = 0;
  public commentCount: number = 0;
  public bookmarkCount: number = 0;
  public reacted: boolean;
  public bookmarked: boolean;
  public blocked: boolean;
  public createdBy: User;
  public createdAt: Date;
  public updatedAt: Date;
  public hashtags: Array<Hashtag> = [];

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.postType) {
        this.postType = PostType[data.postType as string];
      }

      if (data.title) {
        this.title = data.title;
      }

      if (data.subtitle) {
        this.subtitle = data.subtitle;
      }

      if (data.body) {
        this.body = data.body;
        this.formattedBody = this.formatBody(data.body);
      }

      if (data.coverImage) {
        this.coverImage = new Image(data.coverImage);
      }

      if (data.topic) {
        this.topic = new Topic(data.topic);
      }

      if (data.prompt) {
        this.prompt = new Post(data.prompt);
      }

      if (data.hashtagText) {
        this.hashtagText = data.hashtagText;
      }

      if (data.slug) {
        this.slug = data.slug;
      }

      if (data.visibility) {
        this.visibility = Visibility[data.visibility as string];
      }

      if (data.publishStatus) {
        this.publishStatus = PublishStatus[data.publishStatus as string];
      }

      if (data.reactionCount) {
        this.reactionCount = data.reactionCount;
      }

      if (data.commentCount) {
        this.commentCount = data.commentCount;
      }

      if (data.bookmarkCount) {
        this.bookmarkCount = data.bookmarkCount;
      }

      if (data.reacted) {
        this.reacted = data.reacted;
      }

      if (data.bookmarked) {
        this.bookmarked = data.bookmarked;
      }

      if (data.blocked) {
        this.blocked = data.blocked;
      }

      if (data.topic) {
        this.topic = new Topic(data.topic);
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }

      if (data.updatedAt) {
        this.updatedAt = DateUtils.parseDate(data.updatedAt);
      }

      if (data.hashtags) {
        this.hashtags = data.hashtags.map((hashtagData) => new Hashtag(hashtagData));
      }
    }
  }

  isWriting(): boolean {
    return this.postType === PostType.WRITING;
  }

  isPrompt(): boolean {
    return this.postType === PostType.PROMPT;
  }

  isDraft(): boolean {
    return this.publishStatus === PublishStatus.DRAFT;
  }

  isPublished(): boolean {
    return this.publishStatus === PublishStatus.PUBLISHED;
  }

  isPublicVisibility(): boolean {
    return this.visibility === Visibility.PUBLIC;
  }

  isFriendsVisibility(): boolean {
    return this.visibility === Visibility.FRIENDS;
  }

  isPrivateVisibility(): boolean {
    return this.visibility === Visibility.PRIVATE;
  }

  isCreatedBy(user: User) {
    return this.createdBy.entityId === user.entityId;
  }

  isEditableBy(user: User) {
    return this.createdBy.entityId === user.entityId;
  }

  toData(): any {
    const data: any = {};

    if (this.postType != null) {
      data.postType = PostType[this.postType];
    }

    if (this.title) {
      data.title = this.title;
    }

    if (this.subtitle) {
      data.subtitle = this.subtitle;
    }

    if (this.body) {
      data.body = this.body;
    }

    if (this.coverImage) {
      data.coverImageId = this.coverImage.entityId;
    }

    if (this.topic) {
      data.topicId = this.topic.entityId;
    }

    if (this.prompt) {
      data.promptId = this.prompt.entityId;
    }

    if (this.hashtagText) {
      data.hashtagText = this.hashtagText;
    }

    if (this.slug) {
      data.slug = this.slug;
    }

    if (this.visibility != null) {
      data.visibility = Visibility[this.visibility];
    }

    if (this.publishStatus != null) {
      data.publishStatus = PublishStatus[this.publishStatus];
    }

    return data;
  }

  private formatBody(body: string): string {
    return body.replace(/#(\w+)/g, "<a href='/hashtags/$1'>#$1</a>");
  }

}
