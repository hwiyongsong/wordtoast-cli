import { AbstractEntity } from "@app/entities/abstract.entity";
import { User } from "@app/entities/user";
import { DateUtils } from "@app/utils/date-utils";

export class UserFollow extends AbstractEntity {

  public user: User;
  public createdBy: User;
  public createdAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.user) {
        this.user = new User(data.user);
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }
    }
  }

}
