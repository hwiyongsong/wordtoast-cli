import { EntityUtils } from "@app/utils/entity-utils";

export class AbstractEntity {

  public entityUrn: string;
  public entityId: string;
  public entityType: string;
  public entitySubtype: string;
  public invalidated: boolean;

  constructor(data?: any) {
    if (data) {
      if (data.urn) {
        this.entityUrn = data.urn;
        this.entityId = EntityUtils.getEntityId(data.urn);
        this.entityType = EntityUtils.getEntityType(data.urn);
        this.entitySubtype = EntityUtils.getEntitySubtype(data.urn);
      } else {
        this.entityId = data.id;
      }
    }
  }

  isPrototype(): boolean {
    return !this.entityId;
  }

  isPersisted(): boolean {
    return !this.isPrototype();
  }

  isEntityType(entityType): boolean {
    return this.entityType === entityType;
  }

  isEntitySubtype(entitySubtype): boolean {
    return this.entitySubtype === entitySubtype;
  }

}

