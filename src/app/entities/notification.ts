import { AbstractEntity } from "@app/entities/abstract.entity";
import { FriendRequest } from "@app/entities/friend-request";
import { NotificationType } from "@app/entities/notification-type";
import { Post } from "@app/entities/post";
import { PostComment } from "@app/entities/post-comment";
import { PostReaction } from "@app/entities/post-reaction";
import { User } from "@app/entities/user";
import { UserFollow } from "@app/entities/user-follow";
import { DateUtils } from "@app/utils/date-utils";

export class Notification extends AbstractEntity {

  public notificationType: NotificationType;
  public actor: User;
  public friendRequest: FriendRequest;
  public userFollow: UserFollow;
  public post: Post;
  public postReaction: PostReaction;
  public postComment: PostComment;
  public read: boolean;
  public createdAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.notificationType) {
        this.notificationType = NotificationType[data.notificationType as string];
      }

      if (data.actor) {
        this.actor = new User(data.actor);
      }

      if (data.friendRequest) {
        this.friendRequest = new FriendRequest(data.friendRequest);
      }

      if (data.userFollow) {
        this.userFollow = new UserFollow(data.userFollow);
      }

      if (data.post) {
        this.post = new Post(data.post);
      }

      if (data.postReaction) {
        this.postReaction = new PostReaction(data.postReaction);
      }

      if (data.postComment) {
        this.postComment = new PostComment(data.postComment);
      }

      if (data.read) {
        this.read = data.read;
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }
    }
  }

  isFriendRequestCreate() {
    return this.notificationType === NotificationType.FRIEND_REQUEST_CREATE;
  }

  isFriendRequestAccept() {
    return this.notificationType === NotificationType.FRIEND_REQUEST_ACCEPT;
  }

  isUserFollow() {
    return this.notificationType === NotificationType.USER_FOLLOW;
  }

  isPostReact() {
    return this.notificationType === NotificationType.POST_REACT;
  }

}
