import { AbstractEntity } from "@app/entities/abstract.entity";
import { Post } from "@app/entities/post";
import { User } from "@app/entities/user";
import { DateUtils } from "@app/utils/date-utils";

export class PostBlock extends AbstractEntity {

  public post: Post;
  public createdBy: User;
  public createdAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.post) {
        this.post = new Post(data.user);
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }
    }
  }

}
