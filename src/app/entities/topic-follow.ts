import { AbstractEntity } from "@app/entities/abstract.entity";
import { Topic } from "@app/entities/topic";
import { User } from "@app/entities/user";
import { DateUtils } from "@app/utils/date-utils";

export class TopicFollow extends AbstractEntity {

  public topic: Topic;
  public createdBy: User;
  public createdAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.topic) {
        this.topic = new Topic(data.topic);
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }
    }
  }

}
