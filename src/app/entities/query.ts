export class Query {

  private readonly parameters: any = {};

  constructor(data?: any) {
    this.limit(10);
    this.offset(0);

    if (data) {
      this.parameters = {...this.parameters, ...data};
    }
  }

  put(key: string, value: any): Query {
    this.parameters[key] = value;
    return this;
  }

  get(key: string): Query {
    return this.parameters[key];
    return this;
  }

  limit(value: number): Query {
    this.parameters.limit = value;
    return this;
  }

  offset(value: number): Query {
    this.parameters.offset = value;
    return this;
  }

  toData() {
    return this.parameters;
  }

}
