export enum NotificationType {

  FRIEND_REQUEST_ACCEPT,
  FRIEND_REQUEST_CREATE,
  POST_REACT,
  USER_FOLLOW,

}
