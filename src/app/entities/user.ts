import { AbstractEntity } from "@app/entities/abstract.entity";
import { Image } from "@app/entities/image";
import { PlanType } from "@app/entities/plan-type";
import { RoleType } from "@app/entities/role-type";
import { DateUtils } from "@app/utils/date-utils";

export class User extends AbstractEntity {

  public displayName: string;
  public headline: string;
  public email: string;
  public emailVerified: boolean;
  public phoneNumber: string;
  public phoneNumberVerified: boolean;
  public profileImage: Image;
  public firebaseUserId: string;
  public facebookUserId: string;
  public handle: string;
  public roleType: RoleType;
  public planType: PlanType;
  public referralCode: string;
  public referredCode: string;
  public totalPoints: number = 0;
  public followerCount: number = 0;
  public followingCount: number = 0;
  public friendCount: number = 0;
  public following: boolean;
  public friended: boolean;
  public friending: boolean;
  public editable: boolean;
  public blocked: boolean;
  public createdAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.displayName) {
        this.displayName = data.displayName;
      }

      if (data.headline) {
        this.headline = data.headline;
      }

      if (data.email) {
        this.email = data.email;
      }

      if (data.emailVerified) {
        this.emailVerified = data.emailVerified;
      }

      if (data.phoneNumber) {
        this.phoneNumber = data.phoneNumber;
      }

      if (data.phoneNumberVerified) {
        this.phoneNumberVerified = data.phoneNumberVerified;
      }

      if (data.profileImage) {
        this.profileImage = new Image(data.profileImage);
      }

      if (data.handle) {
        this.handle = data.handle;
      }

      if (data.firebaseUserId) {
        this.firebaseUserId = data.firebaseUserId;
      }

      if (data.facebookUserId) {
        this.facebookUserId = data.facebookUserId;
      }

      if (data.roleType) {
        this.roleType = RoleType[data.roleType as string];
      }

      if (data.planType) {
        this.planType = PlanType[data.planType as string];
      }

      if (data.referralCode) {
        this.referralCode = data.referralCode;
      }

      if (data.totalPoints) {
        this.totalPoints = data.totalPoints;
      }

      if (data.followerCount) {
        this.followerCount = data.followerCount;
      }

      if (data.followingCount) {
        this.followingCount = data.followingCount;
      }

      if (data.friendCount) {
        this.friendCount = data.friendCount;
      }

      if (data.following) {
        this.following = data.following;
      }

      if (data.friended) {
        this.friended = data.friended;
      }

      if (data.friending) {
        this.friending = data.friending;
      }

      if (data.editable) {
        this.editable = data.editable;
      }

      if (data.blocked) {
        this.blocked = data.blocked;
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }
    }
  }

  isGuest() {
    return this.roleType === RoleType.GUEST;
  }

  isMember() {
    return this.roleType === RoleType.MEMBER;
  }

  isAdmin() {
    return this.roleType === RoleType.ADMIN;
  }

  isSystem() {
    return this.roleType === RoleType.SYSTEM;
  }

  isFreePlan() {
    return this.planType === PlanType.FREE;
  }

  isPremiumPlan() {
    return this.planType === PlanType.PREMIUM;
  }

  toData() {
    const data: any = {};

    if (this.displayName) {
      data.displayName = this.displayName;
    }

    if (this.headline) {
      data.headline = this.headline;
    }

    if (this.email) {
      data.email = this.email;
    }

    if (this.phoneNumber) {
      data.phoneNumber = this.phoneNumber;
    }

    if (this.profileImage) {
      data.profileImageId = this.profileImage.entityId;
    }

    if (this.handle) {
      data.handle = this.handle;
    }

    if (this.firebaseUserId) {
      data.firebaseUserId = this.firebaseUserId;
    }

    if (this.facebookUserId) {
      data.facebookUserId = this.facebookUserId;
    }

    if (this.referredCode) {
      data.referredCode = this.referredCode;
    }

    return data;
  }

}
