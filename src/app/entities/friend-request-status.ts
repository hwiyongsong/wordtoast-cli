export enum FriendRequestStatus {

  ACCEPTED,
  DECLINED,
  PENDING

}
