import { ChromeHeader } from "@app/entities/chrome-header";
import { Image } from "@app/entities/image";
import { NavItem } from "@app/entities/nav-item";
import { Notification } from "@app/entities/notification";
import { Post } from "@app/entities/post";
import { PostBookmark } from "@app/entities/post-bookmark";
import { PostComment } from "@app/entities/post-comment";
import { PostReaction } from "@app/entities/post-reaction";
import { Topic } from "@app/entities/topic";
import { TopicFollow } from "@app/entities/topic-follow";
import { User } from "@app/entities/user";
import { UserFollow } from "@app/entities/user-follow";
import { UserFriend } from "@app/entities/user-friend";

export class Entity {

  static of(data: any): any {
    if (data.urn.startsWith("urn:image:")) {
      return new Image(data);
    }

    if (data.urn.startsWith("urn:navigation:")) {
      return new ChromeHeader(data);
    }

    if (data.urn.startsWith("urn:navigation-item:")) {
      return new NavItem(data);
    }

    if (data.urn.startsWith("urn:notification:")) {
      return new Notification(data);
    }

    if (data.urn.startsWith("urn:post:")) {
      return new Post(data);
    }

    if (data.urn.startsWith("urn:post-bookmark:")) {
      return new PostBookmark(data);
    }

    if (data.urn.startsWith("urn:post-comment:")) {
      return new PostComment(data);
    }

    if (data.urn.startsWith("urn:post-reaction:")) {
      return new PostReaction(data);
    }

    if (data.urn.startsWith("urn:topic:")) {
      return new Topic(data);
    }

    if (data.urn.startsWith("urn:topic-follow:")) {
      return new TopicFollow(data);
    }

    if (data.urn.startsWith("urn:user:")) {
      return new User(data);
    }

    if (data.urn.startsWith("urn:user-follow:")) {
      return new UserFollow(data);
    }

    if (data.urn.startsWith("urn:user-friend:")) {
      return new UserFriend(data);
    }

    throw new Error("Unsupported navItem: " + data.urn);
  }

}
