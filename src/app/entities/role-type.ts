export enum RoleType {

  ADMIN,
  GUEST,
  MEMBER,
  SYSTEM

}
