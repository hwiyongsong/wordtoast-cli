import { AbstractEntity } from "@app/entities/abstract.entity";
import { Post } from "@app/entities/post";
import { ReactionType } from "@app/entities/reaction-type";
import { User } from "@app/entities/user";
import { DateUtils } from "@app/utils/date-utils";

export class PostReaction extends AbstractEntity {

  public post: Post;
  public reactionType: ReactionType;
  public createdBy: User;
  public createdAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.post) {
        this.post = new Post(data.post);
      }

      if (data.reactionType) {
        this.reactionType = ReactionType[data.reactionType as string];
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }
    }
  }

  isLike() {
    return this.reactionType === ReactionType.LIKE;
  }

  toData(): any {
    const data: any = {};

    if (this.reactionType != null) {
      data.reactionType = ReactionType[this.reactionType];
    }

    return data;
  }

}
