import { AbstractEntity } from "@app/entities/abstract.entity";
import { Post } from "@app/entities/post";
import { User } from "@app/entities/user";

export class SearchResult extends AbstractEntity {

  public input: string;
  public users: Array<User> = [];
  public posts: Array<Post> = [];
  public loaded: boolean = false;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.input) {
        this.input = data.input;
      }

      if (data.users) {
        this.users = data.users.map((userData) => new User(userData));
      }

      if (data.posts) {
        this.posts = data.posts.map((postData) => new Post(postData));
      }

      this.loaded = true;
    }
  }

  size(): number {
    return this.users.length + this.posts.length;
  }

  isEmpty(): boolean {
    return this.size() === 0;
  }

  clear() {
    this.input = "";
    this.users = [];
    this.posts = [];
    this.loaded = false;
  }

}
