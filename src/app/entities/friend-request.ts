import { RequestParameter } from "@app/core/request-parameter";
import { AbstractEntity } from "@app/entities/abstract.entity";
import { FriendRequestStatus } from "@app/entities/friend-request-status";
import { User } from "@app/entities/user";
import { DateUtils } from "@app/utils/date-utils";

export class FriendRequest extends AbstractEntity {

  public user: User;
  public notes: string;
  public status: FriendRequestStatus;
  public createdBy: User;
  public createdAt: Date;
  public respondedAt: Date;

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.user) {
        this.user = new User(data.user);
      }

      if (data.notes) {
        this.notes = data.notes;
      }

      if (data.status) {
        this.status = FriendRequestStatus[data.status as string];
      }

      if (data.createdBy) {
        this.createdBy = new User(data.createdBy);
      }

      if (data.createdAt) {
        this.createdAt = DateUtils.parseDate(data.createdAt);
      }

      if (data.respondedAt) {
        this.respondedAt = DateUtils.parseDate(data.respondedAt);
      }
    }
  }

  isPending() {
    return this.status === FriendRequestStatus.PENDING;
  }

  isAccepted() {
    return this.status === FriendRequestStatus.ACCEPTED;
  }

  isDeclined() {
    return this.status === FriendRequestStatus.DECLINED;
  }

  toData() {
    const data: any = {};

    data[RequestParameter.USER_ID] = this.user.entityId;
    data[RequestParameter.NOTES] = this.notes;

    return data;
  }

}
