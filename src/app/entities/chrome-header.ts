import { AbstractEntity } from "@app/entities/abstract.entity";
import { NavItem } from "@app/entities/nav-item";

export class ChromeHeader extends AbstractEntity {

  public navItems: Array<NavItem> = [];

  constructor(data?: any) {
    super(data);

    if (data) {
      if (data.navItems) {
        this.navItems = data.navItems.map((navItemData) => new NavItem(navItemData));
      }
    }
  }

}
