import { Directive, ElementRef, HostListener, OnInit } from "@angular/core";

/* Based on https://github.com/flivni/ionic2-autosize */

@Directive({
  selector: "ion-textarea[autosize='true']"
})
export class TextareaAutosizeDirective implements OnInit {

  @HostListener("input", ["$event.target"])
  onInput(textArea: HTMLTextAreaElement): void {
    this.adjustHeight();
  }

  @HostListener("focusout", ["$event.target"])
  onBlur(textArea: HTMLTextAreaElement): void {
    this.adjustHeight();
  }

  constructor(public element: ElementRef) {
    // Do nothing.
  }

  ngOnInit(): void {
    // When initialized with body, the textarea height is not adjusted to account to fit the entire body.
    // To minimize the perceptible latency, we call adjustHeight multiple times with various timeouts.
    setTimeout(() => this.adjustHeight(), 0);
    setTimeout(() => this.adjustHeight(), 100);
    setTimeout(() => this.adjustHeight(), 250);
    setTimeout(() => this.adjustHeight(), 1000);
  }

  private adjustHeight(): void {
    const textArea = this.element.nativeElement.querySelector("textarea");
    if (textArea) {
      textArea.style.overflow = "hidden";

      /*
       * https://forum.ionicframework.com/t/solved-ion-textarea-resize-height-dynamically/80885/28
       * This forces the height to be recalculated when body is deleted.
       */
      textArea.style.height = "auto";
      textArea.style.height = textArea.scrollHeight + "px";
    }
  }

}
