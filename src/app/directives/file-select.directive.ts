import { Directive, ElementRef, EventEmitter, HostListener, Output } from "@angular/core";

@Directive({
  selector: "[fileSelect]"
})
export class FileSelectDirective {

  @Output() selected = new EventEmitter<any>();

  constructor(private element: ElementRef) {
    // Do nothing.
  }

  @HostListener("change")
  public onChange(): any {
    const files = this.element.nativeElement.files;

    if (files && files.length > 0) {
      this.selected.emit(files[0]);
    }
  }

}
