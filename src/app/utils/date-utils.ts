export class DateUtils {

  static now(): Date {
    return new Date();
  }

  static parseDate(str: string) {
    return new Date(str);
  }

}
