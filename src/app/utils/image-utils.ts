import * as EXIF from "exif-js";

export class ImageUtils {

  static resize(imageUrl, maxWidth, maxHeight, onSuccess, onError) {
    const canvas = document.createElement("canvas");
    const image = new Image();

    let exif = null;
    try {
      exif = EXIF.readFromBinaryFile(ImageUtils.base64ToArrayBuffer(imageUrl));
    } catch (error) {
      console.log("Unable to read EXIF: " + error);
    }

    image.onload = () => {
      let width = image.width;
      let height = image.height;

      if (width > height) {
        if (width > maxWidth) {
          height *= maxWidth / width;
          width = maxWidth;
        }
      } else {
        if (height > maxHeight) {
          width *= maxHeight / height;
          height = maxHeight;
        }
      }

      if (exif && ([5, 6, 7, 8].indexOf(exif.Orientation) > -1)) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }

      const ctx = canvas.getContext("2d");

      if (exif) {
        switch (exif.Orientation) {
          case 2:
            ctx.transform(-1, 0, 0, 1, width, 0);
            break;
          case 3:
            ctx.transform(-1, 0, 0, -1, width, height);
            break;
          case 4:
            ctx.transform(1, 0, 0, -1, 0, height);
            break;
          case 5:
            ctx.transform(0, 1, 1, 0, 0, 0);
            break;
          case 6:
            ctx.transform(0, 1, -1, 0, height, 0);
            break;
          case 7:
            ctx.transform(0, -1, -1, 0, height, width);
            break;
          case 8:
            ctx.transform(0, -1, 1, 0, 0, width);
            break;
          default:
            ctx.transform(1, 0, 0, 1, 0, 0);
        }
      }

      ctx.drawImage(image, 0, 0, width, height);

      const dataUrl = canvas.toDataURL("image/jpeg");
      onSuccess(dataUrl);
    };

    image.onerror = (error) => {
      onError(error);
    };

    image.src = imageUrl;
  }

  static base64ToArrayBuffer(base64: string) {
    const binaryString = window.atob(base64.split(",")[1]);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
  }

}
