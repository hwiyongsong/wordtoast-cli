export class StringUtils {

  static contains(str: string, substring: string) {
    if (str && substring) {
      return str.indexOf(substring) > -1;
    }

    return false;
  }

  static containsIgnoreCase(str: string, substring: string) {
    if (str && substring) {
      return str.toLowerCase().indexOf(substring.toLowerCase()) > -1;
    }

    return false;
  }

  static countWords(str: string) {
    str = str || "";
    str = str.trim();
    return str ? str.split(/\s+/).length : 0;
  }

  static truncate(str: string, maxCharacters: number = 100): string {
    if (str) {
      str += " ";
      str = str.substr(0, maxCharacters);
      return str.substr(0, Math.min(str.length, str.lastIndexOf(" "))).trim();
    }

    return str;
  }

}
