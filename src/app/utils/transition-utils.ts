import { createAnimation } from "@ionic/core";
import { Animation } from "@ionic/core/dist/types/utils/animation/animation-interface";

export class TransitionUtils {

  static noAnimation(element: any, options?: any): Animation {
    return createAnimation();
  }

}
