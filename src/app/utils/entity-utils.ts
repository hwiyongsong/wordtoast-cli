import * as shortUuid from "short-uuid";

export class EntityUtils {

  static getEntityId(entityUrn: string): string {
    const tokens = entityUrn.split(":");
    return tokens[tokens.length - 1];
  }

  static getEntityType(entityUrn: string): string {
    const tokens = entityUrn.split(":");
    return tokens[1];
  }

  static getEntitySubtype(entityUrn: string): string {
    const tokens = entityUrn.split(":");
    return tokens.length > 3 ? tokens[2] : null;
  }

  static generateFriendlyId() {
    return shortUuid.generate();
  }

  static getIndexOf(array: any[], element: any): number {
    for (let i = 0; i < array.length ; i++) {
      if (array[i].entityUrn === element.entityUrn) {
        return i;
      }
    }

    return -1;
  }

  static equals(object1: any, object2: any) {
    return object1 && object2 ? object1.entityId === object2.entityId : object1 === object2;
  }

}
