import { Component, Input, OnInit } from "@angular/core";
import { SharePopoverBlock } from "@app/blocks/share-popover/share-popover.block";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostService } from "@app/services/post.service";
import { UserService } from "@app/services/user.service";
import { AlertController, ModalController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-post-popover-block",
  templateUrl: "./post-popover.block.html",
  styleUrls: ["./post-popover.block.scss"],
})
export class PostPopoverBlock implements OnInit {

  @Input() post: Post;
  public editable: boolean;

  constructor(private navigation: Navigation,
              public session: Session,
              private tracking: Tracking,
              private alertController: AlertController,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private postService: PostService,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackBlockImpression("app-post-popover-block");
    this.editable = this.session.authenticated && (this.post.isCreatedBy(this.session.user) || this.session.user.isAdmin());
  }

  async onEditPost() {
    this.popoverController.dismiss();

    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        post: this.post
      },
      cssClass: "full"
    });

    return await modal.present();
  }

  async onRemovePost() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Delete post",
      message: "This post will be gone forever. Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.postService.removePost(this.post).subscribe();
          }
        }
      ]
    });

    await alert.present();
  }

  async onSharePost() {
    this.popoverController.dismiss();

    const popover = await this.popoverController.create({
      component: SharePopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onHidePost() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Hide post",
      message: "Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.postService.createPostBlock(this.post).subscribe();
          }
        }
      ]
    });

    await alert.present();
  }

  async onReportPost() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "What would you like to report?",
      inputs: [
        {
          name: "notes",
          type: "textarea",
          placeholder: "Write a message..."
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "medium"
        }, {
          text: "Send",
          handler: (data) => {
            // TODO: Implement this.
          }
        }
      ]
    });

    alert.present();
  }

  async onBlockUser() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Block " + this.post.createdBy.displayName,
      message: "Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.userService.createUserBlock(this.post.createdBy).subscribe();
          }
        }
      ]
    });

    await alert.present();
  }

}
