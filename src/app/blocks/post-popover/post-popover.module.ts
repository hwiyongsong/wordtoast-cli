import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";

@NgModule({
  declarations: [
    PostPopoverBlock
  ],
  entryComponents: [
    PostFormModal
  ],
  imports: [
    AppCommonModule,
    PostFormModalModule
  ],
  exports: [
    PostPopoverBlock
  ]
})
export class PostPopoverBlockModule {
}
