import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ComingSoonBlock } from "@app/blocks/coming-soon/coming-soon.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    ComingSoonBlock
  ],
  exports: [
    ComingSoonBlock
  ]
})
export class ComingSoonBlockModule {
}
