import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PostBlock } from "@app/blocks/post/post.block";
import { PromptBlockModule } from "@app/blocks/prompt/prompt.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { WritingBlockModule } from "@app/blocks/writing/writing.module";

@NgModule({
  imports: [
    AppCommonModule,
    PromptBlockModule,
    WritingBlockModule,
    UserAvatarBlockModule,
    ImageBlockModule
  ],
  declarations: [
    PostBlock
  ],
  exports: [
    PostBlock
  ]
})
export class PostBlockModule {
}
