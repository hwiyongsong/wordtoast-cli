import { Component, Input, OnInit } from "@angular/core";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { SharePopoverBlock } from "@app/blocks/share-popover/share-popover.block";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { PostType } from "@app/entities/post-type";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostService } from "@app/services/post.service";
import { ModalController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-post-block",
  templateUrl: "./post.block.html",
  styleUrls: ["./post.block.scss"],
})
export class PostBlock implements OnInit {

  @Input() post: Post;
  @Input() layout: string = "feed-item";

  constructor(public chrome: Chrome,
              private navigation: Navigation,
              private tracking: Tracking,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private postService: PostService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-writing-block");
  }

  onUserDetail() {
    this.navigation.goUserDetailPage(this.post.createdBy.entityId);
  }

  onPostDetail() {
    this.navigation.goPostDetailPage(this.post.entityId);
  }

  onPromptDetail() {
    this.navigation.goPostDetailPage(this.post.prompt.entityId);
  }

  onTopicDetail() {
    this.navigation.goTopicDetailPage(this.post.topic.entityId);
  }

  onCreateReaction() {
    this.postService.createPostReaction(this.post).subscribe();
  }

  onRemoveReaction() {
    this.postService.removePostReaction(this.post).subscribe();
  }

  onCreateBookmark() {
    this.postService.createPostBookmark(this.post).subscribe();
  }

  onRemoveBookmark() {
    this.postService.removePostBookmark(this.post).subscribe();
  }

  onAuthenticate() {
    this.navigation.goWelcomePage();
  }

  async onWrite() {
    const post = new Post();
    post.postType = PostType.WRITING;

    if (this.post.isPrompt()) {
      post.prompt = this.post;
      post.topic = this.post.topic;
    }

    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        post: post
      },
      cssClass: "full"
    });

    return await modal.present();
  }

  async onShare(event) {
    const popover = await this.popoverController.create({
      component: SharePopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onMore(event) {
    const popover = await this.popoverController.create({
      component: PostPopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

}
