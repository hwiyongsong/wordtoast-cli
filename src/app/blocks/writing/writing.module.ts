import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { PostPopoverBlockModule } from "@app/blocks/post-popover/post-popover.module";
import { SharePopoverBlock } from "@app/blocks/share-popover/share-popover.block";
import { SharePopoverBlockModule } from "@app/blocks/share-popover/share-popover.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { WritingBlock } from "@app/blocks/writing/writing.block";

@NgModule({
  declarations: [
    WritingBlock
  ],
  entryComponents: [
    PostPopoverBlock,
    SharePopoverBlock
  ],
  imports: [
    AppCommonModule,
    ImageBlockModule,
    PostPopoverBlockModule,
    SharePopoverBlockModule,
    UserAvatarBlockModule
  ],
  exports: [
    WritingBlock
  ]
})
export class WritingBlockModule {
}
