import { Component, Input, OnInit } from "@angular/core";
import { PostPopoverBlock } from "@app/blocks/post-popover/post-popover.block";
import { SharePopoverBlock } from "@app/blocks/share-popover/share-popover.block";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { PostService } from "@app/services/post.service";
import { ModalController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-writing-block",
  templateUrl: "./writing.block.html",
  styleUrls: ["./writing.block.scss"],
})
export class WritingBlock implements OnInit {

  @Input() post: Post;
  @Input() layout: string = "feed-item";

  constructor(private navigation: Navigation,
              private tracking: Tracking,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private postService: PostService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-writing-block");
  }

  onUserDetail() {
    this.navigation.goUserDetailPage(this.post.createdBy.entityId);
  }

  onPostDetail() {
    this.navigation.goPostDetailPage(this.post.entityId);
  }

  onPromptDetail() {
    this.navigation.goPostDetailPage(this.post.prompt.entityId);
  }

  onTopicDetail() {
    this.navigation.goTopicDetailPage(this.post.topic.entityId);
  }

  onCreateReaction() {
    this.postService.createPostReaction(this.post).subscribe();
  }

  onRemoveReaction() {
    this.postService.removePostReaction(this.post).subscribe();
  }

  onCreateBookmark() {
    this.postService.createPostBookmark(this.post).subscribe();
  }

  onRemoveBookmark() {
    this.postService.removePostBookmark(this.post).subscribe();
  }

  async onShare(event) {
    const popover = await this.popoverController.create({
      component: SharePopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

  async onMore(event) {
    const popover = await this.popoverController.create({
      component: PostPopoverBlock,
      componentProps: {
        post: this.post
      },
      event: event
    });

    return await popover.present();
  }

}
