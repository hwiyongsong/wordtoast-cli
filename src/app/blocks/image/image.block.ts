import { Component, Input, OnInit } from "@angular/core";
import { Tracking } from "@app/core/tracking";
import { Image } from "@app/entities/image";

@Component({
  selector: "app-image-block",
  templateUrl: "./image.block.html",
  styleUrls: ["./image.block.scss"],
})
export class ImageBlock implements OnInit {

  @Input() image: Image;
  public loaded: boolean = false;

  constructor(private tracking: Tracking) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-image-block", {
      "imageId": this.image.entityId
    });
  }

  setLoaded(loaded: boolean) {
    setTimeout(() => {
      this.loaded = loaded;
    }, 0);
  }

}
