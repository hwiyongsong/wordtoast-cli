import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlock } from "@app/blocks/image/image.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    ImageBlock
  ],
  exports: [
    ImageBlock
  ]
})
export class ImageBlockModule {
}
