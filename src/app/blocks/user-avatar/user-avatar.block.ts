import { Component, Input, OnInit } from "@angular/core";
import { User } from "@app/entities/user";
import { Tracking } from "@app/core/tracking";

@Component({
  selector: "app-user-avatar-block",
  templateUrl: "./user-avatar.block.html",
  styleUrls: ["./user-avatar.block.scss"],
})
export class UserAvatarBlock implements OnInit {

  @Input() user: User;
  @Input() size: string;

  constructor(private tracking: Tracking) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-user-avatar-block", {
      "userId": this.user.entityId
    });
  }

}
