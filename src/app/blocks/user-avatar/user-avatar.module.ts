import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { UserAvatarBlock } from "@app/blocks/user-avatar/user-avatar.block";

@NgModule({
  imports: [
    AppCommonModule,
    ImageBlockModule
  ],
  declarations: [
    UserAvatarBlock
  ],
  exports: [
    UserAvatarBlock
  ]
})
export class UserAvatarBlockModule {
}
