import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { PostCommentPopoverBlock } from "@app/blocks/post-comment-popover/post-comment-popover.block";
import { PostCommentPopoverBlockModule } from "@app/blocks/post-comment-popover/post-comment-popover.module";
import { PostCommentBlock } from "@app/blocks/post-comment/post-comment.block";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";

@NgModule({
  imports: [
    AppCommonModule,
    PostCommentPopoverBlockModule,
    UserAvatarBlockModule
  ],
  exports: [
    PostCommentBlock
  ],
  declarations: [
    PostCommentBlock
  ],
  entryComponents: [
    PostCommentPopoverBlock
  ]
})
export class PostCommentBlockModule {
}
