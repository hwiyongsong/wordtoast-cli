import { Component, Input } from "@angular/core";
import { PostCommentPopoverBlock } from "@app/blocks/post-comment-popover/post-comment-popover.block";
import { Navigation } from "@app/core/navigation";
import { Post } from "@app/entities/post";
import { PostComment } from "@app/entities/post-comment";
import { PopoverController } from "@ionic/angular";

@Component({
  selector: "app-post-comment-block",
  templateUrl: "./post-comment.block.html",
  styleUrls: ["./post-comment.block.scss"],
})
export class PostCommentBlock {

  @Input() post: Post;
  @Input() postComment: PostComment;

  constructor(private navigation: Navigation,
              private popoverController: PopoverController) {
    // Do nothing.
  }

  onUserDetail() {
    this.navigation.goUserDetailPage(this.postComment.createdBy.entityId);
  }

  async onMore(event) {
    const popover = await this.popoverController.create({
      component: PostCommentPopoverBlock,
      componentProps: {
        post: this.post,
        postComment: this.postComment
      },
      event: event
    });

    return await popover.present();
  }

}
