import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { EntityBlock } from "@app/blocks/entity/entity.block";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { NotificationBlockModule } from "@app/blocks/notification/notification.module";
import { PostBlockModule } from "@app/blocks/post/post.module";
import { UserBlockModule } from "@app/blocks/user/user.module";

@NgModule({
  imports: [
    AppCommonModule,
    ImageBlockModule,
    NotificationBlockModule,
    PostBlockModule,
    UserBlockModule
  ],
  declarations: [
    EntityBlock
  ],
  exports: [
    EntityBlock
  ]
})
export class EntityBlockModule {
}
