import { Component, Input } from "@angular/core";

@Component({
  selector: "app-entity-block",
  templateUrl: "./entity.block.html",
  styleUrls: ["./entity.block.scss"],
})
export class EntityBlock {

  @Input() entity: any;
  @Input() layout: string;

}
