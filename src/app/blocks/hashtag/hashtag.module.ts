import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { HashtagBlock } from "@app/blocks/hashtag/hashtag.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  exports: [
    HashtagBlock
  ],
  declarations: [
    HashtagBlock
  ],
  entryComponents: []
})
export class HashtagBlockModule {
}
