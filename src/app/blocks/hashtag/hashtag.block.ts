import { Component, Input } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Hashtag } from "@app/entities/hashtag";

@Component({
  selector: "app-hashtag-block",
  templateUrl: "./hashtag.block.html",
  styleUrls: ["./hashtag.block.scss"],
})
export class HashtagBlock {

  @Input() hashtag: Hashtag;

  constructor(public chrome: Chrome) {
    // Do nothing.
  }

}
