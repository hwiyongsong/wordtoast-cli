import { Component, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { RequestParameter } from "@app/core/request-parameter";
import { Tracking } from "@app/core/tracking";
import { QueryResult } from "../../entities/query-result";
import { Post } from "@app/entities/post";
import { PostType } from "@app/entities/post-type";
import { Query } from "@app/entities/query";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostService } from "@app/services/post.service";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-popular-prompts-block",
  templateUrl: "./popular-prompts.block.html",
  styleUrls: ["./popular-prompts.block.scss"],
})
export class PopularPromptsBlock implements OnInit {

  public promptsFeed: QueryResult = new QueryResult();
  public promptsQuery: Query = new Query();

  constructor(private navigation: Navigation,
              private tracking: Tracking,
              private modalController: ModalController,
              private postService: PostService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.promptsQuery.put(RequestParameter.POST_TYPE, PostType[PostType.PROMPT]);
    this.promptsQuery.limit(5);

    this.postService.queryPosts(this.promptsQuery).subscribe((promptsFeed) => {
      this.promptsFeed.refresh(promptsFeed);
    });
  }

  onPostDetail(prompt: Post) {
    this.navigation.goPostDetailPage(prompt.entityId);
  }

  async onWrite(prompt: Post) {
    const post = new Post();
    post.postType = PostType.WRITING;
    post.topic = prompt.topic;
    post.prompt = prompt;

    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        post: post
      },
      cssClass: "full"
    });

    return await modal.present();
  }

}
