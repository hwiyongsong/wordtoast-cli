import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PopularPromptsBlock } from "@app/blocks/popular-prompts/popular-prompts.block";
import { PromptBlockModule } from "@app/blocks/prompt/prompt.module";

@NgModule({
  imports: [
    AppCommonModule,
    PromptBlockModule,
    ImageBlockModule
  ],
  declarations: [
    PopularPromptsBlock
  ],
  exports: [
    PopularPromptsBlock
  ]
})
export class PopularPromptsBlockModule {
}
