import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { UserBlock } from "@app/blocks/user/user.block";

@NgModule({
  imports: [
    AppCommonModule,
    UserAvatarBlockModule
  ],
  declarations: [
    UserBlock
  ],
  exports: [
    UserBlock
  ]
})
export class UserBlockModule {
}
