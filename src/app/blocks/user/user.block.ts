import { Component, Input, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { FriendRequest } from "@app/entities/friend-request";
import { User } from "@app/entities/user";
import { UserService } from "@app/services/user.service";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-user-block",
  templateUrl: "./user.block.html",
  styleUrls: ["./user.block.scss"],
})
export class UserBlock implements OnInit {

  @Input() user: User;
  @Input() layout: string;

  constructor(public chrome: Chrome,
              private navigation: Navigation,
              private tracking: Tracking,
              private alertController: AlertController,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-user-block", {
      "userId": this.user.entityId
    });
  }

  goUserDetailPage() {
    this.navigation.goUserDetailPage(this.user.entityId);
  }

  async onCreateFriendRequest() {
    const alert = await this.alertController.create({
      header: "Send friend request to " + this.user.displayName,
      inputs: [
        {
          name: "notes",
          type: "textarea",
          placeholder: "Write a message..."
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "medium"
        }, {
          text: "Send",
          handler: (data) => {
            const friendRequest = new FriendRequest();
            friendRequest.user = this.user;
            friendRequest.notes = data.notes;

            this.userService.createFriendRequest(friendRequest).subscribe(() => {
              this.chrome.presentToast("Friend request sent to " + this.user.displayName);
            });
          }
        }
      ]
    });

    alert.present();
  }

  onCreateUserFollow() {
    this.userService.createUserFollow(this.user).subscribe();
  }

  onRemoveUserFollow() {
    this.userService.removeUserFollow(this.user).subscribe();
  }

}
