import { Component, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { PostType } from "@app/entities/post-type";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-post-start-block",
  templateUrl: "./post-start.block.html",
  styleUrls: ["./post-start.block.scss"],
})
export class PostStartBlock implements OnInit {

  constructor(private navigation: Navigation,
              public session: Session,
              private tracking: Tracking,
              private modalController: ModalController) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-post-start-block");
  }

  async onWriting() {
    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        postType: PostType.WRITING
      }
    });

    return await modal.present();
  }

  async onPrompt() {
    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        postType: PostType.PROMPT
      }
    });

    return await modal.present();
  }

}
