import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { PostStartBlock } from "@app/blocks/post-start/post-start.block";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";

@NgModule({
  imports: [
    AppCommonModule,
    UserAvatarBlockModule,
    PostFormModalModule
  ],
  declarations: [
    PostStartBlock
  ],
  exports: [
    PostStartBlock
  ]
})
export class PostStartBlockModule {
}
