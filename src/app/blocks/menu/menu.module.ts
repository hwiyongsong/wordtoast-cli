import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { MenuBlock } from "@app/blocks/menu/menu.block";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { PostFormModalModule } from "@app/modals/post-form/post-form.module";

@NgModule({
  declarations: [
    MenuBlock
  ],
  entryComponents: [
    PostFormModal
  ],
  imports: [
    AppCommonModule,
    PostFormModalModule,
    UserAvatarBlockModule
  ],
  exports: [
    MenuBlock
  ]
})
export class MenuBlockModule {
}
