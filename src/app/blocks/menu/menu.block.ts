import { Component, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { PostType } from "@app/entities/post-type";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { MenuController, ModalController } from "@ionic/angular";

@Component({
  selector: "app-menu-block",
  templateUrl: "./menu.block.html",
  styleUrls: ["./menu.block.scss"],
})
export class MenuBlock implements OnInit {

  constructor(private navigation: Navigation,
              public session: Session,
              private tracking: Tracking,
              private menuController: MenuController,
              private modalController: ModalController) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-menu-block");
  }

  onUserProfile() {
    this.navigation.goUserDetailPage(this.session.user.entityId);
    this.menuController.close();
  }

  async onCreateWriting() {
    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        postType: PostType.WRITING
      },
      cssClass: "full"
    });

    this.menuController.close();
    return await modal.present();
  }

  async onCreatePrompt() {
    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        postType: PostType.PROMPT
      }
    });

    this.menuController.close();
    return await modal.present();
  }

  goPosts() {
    this.navigation.goMyPostsPage();
    this.menuController.close();
  }

  onBookmarks() {
    this.navigation.goMyBookmarksPage();
    this.menuController.close();
  }

  onInterests() {
    this.navigation.goMyInterestsPage();
    this.menuController.close();
  }

  onInviteFriends() {
    this.navigation.goInvitePage();
    this.menuController.close();
  }

  onSettings() {
    this.navigation.goSettingsPage();
    this.menuController.close();
  }

}
