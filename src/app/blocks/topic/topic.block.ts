import { Component, Input } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Topic } from "@app/entities/topic";
import { TopicService } from "@app/services/topic.service";

@Component({
  selector: "app-topic-block",
  templateUrl: "./topic.block.html",
  styleUrls: ["./topic.block.scss"],
})
export class TopicBlock {

  @Input() topic: Topic;
  @Input() layout: string = "list-item";

  constructor(private navigation: Navigation,
              private topicService: TopicService) {
    // Do nothing.
  }

  onTopicDetail() {
    this.navigation.goTopicDetailPage(this.topic.entityId);
  }

  onCreateFollow() {
    this.topicService.createTopicFollow(this.topic).subscribe();
  }

  onRemoveFollow() {
    this.topicService.removeTopicFollow(this.topic).subscribe();
  }

}
