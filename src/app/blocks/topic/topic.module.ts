import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { TopicBlock } from "@app/blocks/topic/topic.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    TopicBlock
  ],
  exports: [
    TopicBlock
  ]
})
export class TopicBlockModule {
}
