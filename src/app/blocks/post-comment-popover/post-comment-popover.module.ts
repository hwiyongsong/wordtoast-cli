import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { PostCommentPopoverBlock } from "@app/blocks/post-comment-popover/post-comment-popover.block";
import { PostCommentFormModal } from "@app/modals/post-comment-form/post-comment-form.modal";
import { PostCommentFormModalModule } from "@app/modals/post-comment-form/post-comment-form.module";

@NgModule({
  imports: [
    AppCommonModule,
    PostCommentFormModalModule
  ],
  exports: [
    PostCommentPopoverBlock
  ],
  declarations: [
    PostCommentPopoverBlock
  ],
  entryComponents: [
    PostCommentFormModal
  ]
})
export class PostCommentPopoverBlockModule {
}
