import { Component, Input, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { PostComment } from "@app/entities/post-comment";
import { PostCommentFormModal } from "@app/modals/post-comment-form/post-comment-form.modal";
import { PostService } from "@app/services/post.service";
import { UserService } from "@app/services/user.service";
import { AlertController, ModalController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-post-comment-popover-block",
  templateUrl: "./post-comment-popover.block.html",
  styleUrls: ["./post-comment-popover.block.scss"],
})
export class PostCommentPopoverBlock implements OnInit {

  @Input() post: Post;
  @Input() postComment: PostComment;
  public editable: boolean;

  constructor(private navigation: Navigation,
              public session: Session,
              private tracking: Tracking,
              private alertController: AlertController,
              private modalController: ModalController,
              private popoverController: PopoverController,
              private postService: PostService,
              private userService: UserService) {
    // Do nothing.
  }

  ngOnInit() {
    this.tracking.trackBlockImpression("app-post-popover-block");
    this.editable = this.postComment.isEditableBy(this.session.user);
  }

  async onEditPostComment() {
    this.popoverController.dismiss();

    const modal = await this.modalController.create({
      component: PostCommentFormModal,
      componentProps: {
        post: this.post,
        postComment: this.postComment
      }
    });

    return await modal.present();
  }

  async onRemovePostComment() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Delete comment",
      message: "This comment will be gone forever. Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.postService.removePostComment(this.post, this.postComment).subscribe();
          }
        }
      ]
    });

    await alert.present();
  }

  async onReportPostComment() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "What would you like to report?",
      inputs: [
        {
          name: "notes",
          type: "textarea",
          placeholder: "Write a message..."
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "medium"
        }, {
          text: "Send",
          handler: (data) => {
            // TODO: Implement this.
          }
        }
      ]
    });

    alert.present();
  }

  async onBlockUser() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Block " + this.post.createdBy.displayName,
      message: "Are you sure?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        }, {
          text: "Yes",
          handler: () => {
            this.userService.createUserBlock(this.post.createdBy).subscribe();
          }
        }
      ]
    });

    await alert.present();
  }

}
