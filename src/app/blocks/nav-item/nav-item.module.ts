import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { NavItemBlock } from "@app/blocks/nav-item/nav-item.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    NavItemBlock
  ],
  exports: [
    NavItemBlock
  ]
})
export class NavItemBlockModule {
}
