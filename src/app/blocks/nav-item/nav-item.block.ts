import { Component, Input } from "@angular/core";
import { Navigation } from "@app/core/navigation";

@Component({
  selector: "app-nav-item-block",
  templateUrl: "./nav-item.block.html",
  styleUrls: ["./nav-item.block.scss"],
})
export class NavItemBlock {

  @Input() navItem: any;

  constructor(private navigation: Navigation) {
    // Do nothing.
  }

  onTopicDetail(topic: any) {
    this.navigation.goTopicDetailPage(topic.entityId);
  }

}
