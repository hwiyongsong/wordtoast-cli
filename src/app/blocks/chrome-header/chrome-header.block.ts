import { Component, Input, OnInit } from "@angular/core";
import { AppContext } from "@app/app.context";
import { Navigation } from "@app/core/navigation";
import { Session } from "@app/core/session";
import { AuthenticationModal } from "@app/modals/authentication/authentication.modal";
import { MenuController, ModalController } from "@ionic/angular";

@Component({
  selector: "app-chrome-header-block",
  templateUrl: "./chrome-header.block.html",
  styleUrls: ["./chrome-header.block.scss"],
})
export class ChromeHeaderBlock implements OnInit {

  private static readonly rootPaths: Array<string> = [
    "/",
    "/home",
    "/welcome"
  ];

  public withBackButton: boolean = false;
  public withBrandTitle: boolean = false;
  @Input() withMenubar: boolean = true;

  constructor(private context: AppContext,
              private navigation: Navigation,
              public session: Session,
              private modalController: ModalController,
              private menuController: MenuController) {
    // Do nothing.
  }

  ngOnInit() {
    this.withBackButton = this.isNotRootPage();
    this.withBrandTitle = !this.isNotRootPage();
    this.session.refreshNotificationCount();
  }

  async onSignIn() {
    const modal = await this.modalController.create({
      component: AuthenticationModal,
      componentProps: {
        intent: "SIGN_IN_HOME"
      }
    });

    return await modal.present();
  }

  async onSearch() {
    this.navigation.goSearchPage();
  }

  onNotifications(): void {
    this.navigation.goMyNotificationsPage();
  }

  onUserMenu(): void {
    this.menuController.open("userMenu");
  }

  private isNotRootPage(): boolean {
    const path = this.context.getRoutePath();
    return ChromeHeaderBlock.rootPaths.indexOf(path) < 0;
  }

}
