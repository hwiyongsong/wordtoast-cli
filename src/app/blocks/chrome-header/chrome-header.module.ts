import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlockModule } from "@app/blocks/brand/brand.module";
import { ChromeHeaderBlock } from "@app/blocks/chrome-header/chrome-header.block";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";
import { AuthenticationModalModule } from "@app/modals/authentication/authentication.module";

@NgModule({
  declarations: [
    ChromeHeaderBlock
  ],
  imports: [
    AppCommonModule,
    AuthenticationModalModule,
    BrandBlockModule,
    UserAvatarBlockModule
  ],
  exports: [
    ChromeHeaderBlock
  ]
})
export class ChromeHeaderBlockModule {
}
