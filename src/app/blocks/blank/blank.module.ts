import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { BlankBlock } from "@app/blocks/blank/blank.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  exports: [
    BlankBlock
  ],
  declarations: [
    BlankBlock
  ],
  entryComponents: []
})
export class BlankBlockModule {
}
