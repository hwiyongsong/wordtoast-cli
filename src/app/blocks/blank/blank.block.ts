import { Component } from "@angular/core";
import { Navigation } from "@app/core/navigation";

@Component({
  selector: "app-blank-block",
  templateUrl: "./blank.block.html",
  styleUrls: ["./blank.block.scss"],
})
export class BlankBlock {

  constructor(private navigation: Navigation) {
    // Do nothing.
  }

}
