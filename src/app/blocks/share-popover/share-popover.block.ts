import { Component, Input, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { AlertController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-share-popover-block",
  templateUrl: "./share-popover.block.html",
  styleUrls: ["./share-popover.block.scss"],
})
export class SharePopoverBlock implements OnInit {

  @Input() post: Post;

  constructor(private navigation: Navigation,
              private tracking: Tracking,
              private alertController: AlertController,
              private popoverController: PopoverController) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-share-popover-block");
  }

  async onShareOnFacebook() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Coming soon!",
      buttons: ["OK"]
    });

    await alert.present();
  }

  async onShareOnTwitter() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Coming soon!",
      buttons: ["OK"]
    });

    await alert.present();
  }

  async onCopyLink() {
    this.popoverController.dismiss();

    const alert = await this.alertController.create({
      header: "Coming soon!",
      buttons: ["OK"]
    });

    await alert.present();
  }

}
