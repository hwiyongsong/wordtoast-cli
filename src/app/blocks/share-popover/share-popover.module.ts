import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { SharePopoverBlock } from "@app/blocks/share-popover/share-popover.block";

@NgModule({
  imports: [
    AppCommonModule,
    ImageBlockModule
  ],
  declarations: [
    SharePopoverBlock
  ],
  exports: [
    SharePopoverBlock
  ]
})
export class SharePopoverBlockModule {
}
