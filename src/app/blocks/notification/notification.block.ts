import { Component, Input, OnInit } from "@angular/core";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Notification } from "@app/entities/notification";

@Component({
  selector: "app-notification-block",
  templateUrl: "./notification.block.html",
  styleUrls: ["./notification.block.scss"],
})
export class NotificationBlock implements OnInit {

  @Input() notification: Notification;

  constructor(private navigation: Navigation,
              private tracking: Tracking) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-notification-block");
  }

  onFriendRequestDetail() {
    this.navigation.goFriendRequestDetailPage(this.notification.friendRequest.entityId);
  }

  onUserDetail(userId: string) {
    this.navigation.goUserDetailPage(userId);
  }

  onPostDetail(postId: string) {
    this.navigation.goPostDetailPage(postId);
  }

}
