import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { NotificationBlock } from "@app/blocks/notification/notification.block";
import { UserAvatarBlockModule } from "@app/blocks/user-avatar/user-avatar.module";

@NgModule({
  imports: [
    AppCommonModule,
    UserAvatarBlockModule
  ],
  declarations: [
    NotificationBlock
  ],
  exports: [
    NotificationBlock
  ]
})
export class NotificationBlockModule {
}
