import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { BrandBlock } from "@app/blocks/brand/brand.block";

@NgModule({
  imports: [
    AppCommonModule
  ],
  declarations: [
    BrandBlock
  ],
  exports: [
    BrandBlock
  ]
})
export class BrandBlockModule {
}
