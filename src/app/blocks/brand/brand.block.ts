import { Component } from "@angular/core";
import { Navigation } from "@app/core/navigation";

@Component({
  selector: "app-brand-block",
  templateUrl: "./brand.block.html",
  styleUrls: ["./brand.block.scss"],
})
export class BrandBlock {

  constructor(private navigation: Navigation) {
    // Do nothing.
  }

  onHome() {
    this.navigation.goHomePage();
  }

}
