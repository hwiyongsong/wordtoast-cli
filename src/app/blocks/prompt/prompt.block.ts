import { Component, Input, OnInit } from "@angular/core";
import { Chrome } from "@app/core/chrome";
import { Navigation } from "@app/core/navigation";
import { Tracking } from "@app/core/tracking";
import { Post } from "@app/entities/post";
import { PostType } from "@app/entities/post-type";
import { PostFormModal } from "@app/modals/post-form/post-form.modal";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-prompt-block",
  templateUrl: "./prompt.block.html",
  styleUrls: ["./prompt.block.scss"],
})
export class PromptBlock implements OnInit {

  @Input() post: Post;
  @Input() layout: string = "feed-item";

  constructor(public chrome: Chrome,
              private navigation: Navigation,
              private tracking: Tracking,
              private modalController: ModalController) {
    // Do nothing.
  }

  ngOnInit(): void {
    this.tracking.trackBlockImpression("app-prompt-block");
  }

  onPostDetail() {
    this.navigation.goPostDetailPage(this.post.entityId);
  }

  onAuthenticate() {
    this.navigation.goWelcomePage();
  }

  async onWrite() {
    const post = new Post();
    post.postType = PostType.WRITING;

    if (this.post.isPrompt()) {
      post.prompt = this.post;
      post.topic = this.post.topic;
    }

    const modal = await this.modalController.create({
      component: PostFormModal,
      componentProps: {
        post: post
      },
      cssClass: "full"
    });

    return await modal.present();
  }

}
