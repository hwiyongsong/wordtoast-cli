import { NgModule } from "@angular/core";
import { AppCommonModule } from "@app/app-common.module";
import { ImageBlockModule } from "@app/blocks/image/image.module";
import { PromptBlock } from "@app/blocks/prompt/prompt.block";

@NgModule({
  imports: [
    AppCommonModule,
    ImageBlockModule
  ],
  declarations: [
    PromptBlock
  ],
  exports: [
    PromptBlock
  ]
})
export class PromptBlockModule {
}
