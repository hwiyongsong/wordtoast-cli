import { NgxLoggerLevel } from "ngx-logger";

export const environment = {
  name: "development",
  production: false,
  baseUrl: "http://localhost:4200",
  apiBaseUrl: "http://localhost:9000",
  firebase: {
    config: {
      apiKey: "AIzaSyDcNfI8ANiQj-a4IpSorfxjVRyF-bnF1_4",
      authDomain: "wordtoast-development.firebaseapp.com",
      databaseURL: "https://wordtoast-development.firebaseio.com",
      projectId: "wordtoast-development",
      storageBucket: "wordtoast-development.appspot.com",
      messagingSenderId: "549088850065",
      appId: "1:549088850065:web:7fdfc9c8e36b02dd514f30"
    }
  },
  logging: {
    level: NgxLoggerLevel.DEBUG
  },
  mixpanel: {
    token: ""
  }
};
