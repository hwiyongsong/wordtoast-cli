import { NgxLoggerLevel } from "ngx-logger";

export const environment = {
  name: "production",
  production: true,
  baseUrl: "https://wordtoast.com",
  apiBaseUrl: "https://wordtoast-api-production.herokuapp.com",
  firebase: {
    config: {
      apiKey: "AIzaSyAS4WbwW-9a58JtBsDR48oDPwg0LjA-9k8",
      authDomain: "wordtoast-production.firebaseapp.com",
      databaseURL: "https://wordtoast-production.firebaseio.com",
      projectId: "wordtoast-production",
      storageBucket: "wordtoast-production.appspot.com",
      messagingSenderId: "317770336575",
      appId: "1:317770336575:web:adaa6c34aa71a7452feb8a"
    }
  },
  logging: {
    level: NgxLoggerLevel.ERROR
  },
  mixpanel: {
    token: ""
  }
};
