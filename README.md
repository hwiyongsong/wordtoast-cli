ionic cordova plugin add cordova-plugin-facebook4 --save --variable APP_ID="2481694862087200" --variable APP_NAME="WordToast"

npm install firebase @angular/fire --save

npm install ngx-logger --save

npm install tailwindcss --save-dev
npm install ng-tailwindcss --global

npm install cropperjs --save
npm install exif-js --save

npm install @ionic-native/facebook --save

npm install short-uuid --save

npm install ngx-clipboard --save

npm install quill --save
npm install ngx-quill@7.3.2 --save  // https://github.com/KillerCodeMonkey/ngx-quill/issues/573

ng add @angular/pwa
