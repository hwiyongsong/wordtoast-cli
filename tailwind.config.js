module.exports = {
  theme: {
    extend: {
      boxShadow: {
        default:  "0 1px 3px rgba(0, 0, 0, 0.06)"
      },
      colors: {
        primary: "#9f7aea",
        secondary: "#ff5a60",
        dark: "#333333",
        medium: "rgba(0, 0, 0, 0.5)",
        light: "#f6f6f6",
        placeholder: "rgba(0, 0, 0, 0.125)",
        divider: "rgba(0, 0,  0, 0.1)",
        facebook: "#3c5a99",
        twitter: "#1da1f2"
      },
      fontFamily: {
        ui: "'Marat Sans', sans-serif",
        content: "'Marat Sans', sans-serif",
        brand: "'Luckiest Guy', cursive"
      },
      fontSize: {
        none: 0
      },
      fontWeight: {
        light: 300,
        normal: 400,
        medium: 500,
        bold: 600
      },
      lineHeight: {
        tight: 1.1,
        snug: 1.2,
        normal: 1.3,
        relaxed: 1.4,
        loose: 1.5
      },
      opacity: {
        10: 0.10,
        15: 0.15,
        20: 0.20,
        25: 0.25,
        30: 0.30,
        35: 0.35,
        40: 0.40,
        45: 0.45,
        50: 0.50,
        55: 0.55,
        60: 0.60,
        65: 0.65,
        70: 0.70,
        75: 0.75,
        80: 0.80,
        85: 0.85,
        90: 0.90,
        95: 0.95
      },
      screens: {
        mobile: {
          max: "1023px"
        },
        desktop: {
          min: "1024px"
        }
      },
      spacing: {
        hairline: "1px",
        tiny: "2px",
        gutter: "16px"
      }
    }
  },
  variants: {
    textColor: ["hover"],
    textDecoration: ["hover"]
  },
  plugins: []
}
